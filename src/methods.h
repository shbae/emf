/*
    eMF Copyright 2008-2009 Sung-Hun Bae

    This file is part of eMF.

    eMF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    eMF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with eMF.  If not, see <http://www.gnu.org/licenses/>
*/

#ifndef _METHOD_H_
#define _METHOD_H_

#include "dataset.h"

using namespace std;

void Load_Methods (dataset &);

void optimize_Drr (dataset &);

//----- Random Simplex (GSL)
double randsimplex (void *);
//----- Simplex (GSL)
double simplex (void *);

//----- Quasi random Search
double qrand (void *);
//----- Random Search
double rand (void *);

//----- Grid Search
double grid (void *);
    void recursive_grid (gsl_vector *,double *,gsl_vector *,int,void *);



// Levenberg-Marquardt (GSL)
double levmar (void *);

// Levenberg-Marquardt (ML)
double levmarML (void *);
double levmarML_bc (void *);

// box constrained minimization (OOL)
double bc_ool (void *);

// conjugate gradient miminization (GSL)
double conjgr (void *);

// Simulated Annealing (GSL)
double sa (void *);

// function for grid search, simplex, and simulated annealing (GSL)
  double f_chisq (const gsl_vector *, void *);

// functions for Levenberg-Marquardt (GSL)
  int lm_f	(const gsl_vector *,void *,gsl_vector *);
  int lm_df	(const gsl_vector *,void *,gsl_matrix *);
  int lm_fdf	(const gsl_vector *,void *,gsl_vector *,gsl_matrix *);

// functions for Levenberg-Marquardt (ML)
  void lm_ML_f  (double *v, double *f, int nv, int nx, void *data);
  void lm_ML_df (double *v, double *J, int nv, int nx, void *data);

// functions for multidimensional minimization (GSL & OOL)
double mmin_f	(const gsl_vector *,void *);
  void mmin_df  (const gsl_vector *,void *,gsl_vector *);
  void mmin_fdf (const gsl_vector *,void *,double *,gsl_vector *);
  void mmin_Hv  (const gsl_vector *,void *,const gsl_vector *,gsl_vector *);

#endif
