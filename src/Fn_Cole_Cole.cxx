/*
    eMF Copyright 2008-2009 Sung-Hun Bae

    This file is part of eMF.

    eMF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    eMF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with eMF.  If not, see <http://www.gnu.org/licenses/>
*/

/*
  Cole-Cole spectral density function
  Based on the Classical Lipari-Szabo Formalism (Model 2 & 4)
  e_e assumed to be 1

  Buevich and Baum (1999) J. Am. Chem. Soc. 121:8671-8672
  Buevich, Shinde, Inouye and Baum (2001) J. Biomolecular NMR 20:233-249
*/

#include "dataset.h"
#include "funclib.h"
#include "util.h"

using namespace std;

void Cole_Cole (const vecd &x, const veci &z, const vecd &p, const vecb &b,
	const void *data, vecd &y, vecd &dyda)
{
dataset *D = (dataset *) data;
double x_min,x_max;

relaxation (&Jw_Cole_Cole,x,z,p,b,data,y,dyda);

// Quadratic Scaling of Rex
D->get_x_range (x_min,x_max);
int np = p.size();
for (int i=0;i<x.size();i++) {
	if (z[i] == 1) {
		y[i] = y[i] + p[4]*SQR(x[i]/x_min); // R2 = R20 + Rex 
		if (dyda.size() > 0) 
			dyda[np*i+4] = SQR(x[i]/x_min);
		}
	}
}

void Jw_Cole_Cole (const vecd &x, const vecd &p, const vecb &b,
	const void *data, vecd &jw, vecd &der)
{
  double te,S2,tc,e,Rex,t,w;
  double s,c,ch2,sh2,ch3,sh3,g1,g2,f0,f1,f2,f3;
  int d = der.size();

  tc = p[0];  // ns
  e  = p[1];
  S2 = p[2];
  te = p[3];  // ns
  Rex= p[4];

  f1 = M_PI_2*(1.-e);
  c = cos(f1);
  s = sin(f1);

  int nx = x.size();

  for (int i=0;i<nx;i++) {
    // w: 1e+9 rad/s
    // spectral density is not defined at w = 0
    // assume J(0) == J(1)

    w = (fabs (x[i]) < 1.e-9 ? 1.e-9 : fabs(x[i]));

    g1 = log(w*tc);
    f0 = 0.2e-9/w;
    f2 = g1*e;
    ch2 = cosh(f2);
    sh2 = sinh(f2);

    if (te > 1.0e-4) { // te > 0.1 ps

      t = te*tc/(te+tc); // 1e-9 s
      f3  = log(w*t);
      ch3 = cosh(f3);
      sh3 = sinh(f3);

      jw[i] = f0*(S2*c/(ch2+s)+(1.0-S2)/ch3); // (s/rad)

    if (d != 0) {
      // tc
      der[5*i+0] = f0*(-e*sh2*c*S2/(tc*SQR(s+ch2))
	      -(te-t)*w*sh3*(1.-S2)/(tc*te*w*SQR(ch3)));
      // e
      der[5*i+1] = f0*(M_PI_2*S2*s/(s+ch2)-c*S2*(g1*sh2-M_PI_2*c)/SQR(s+ch2));
      // S2
      der[5*i+2] = f0*((c/(ch2+s)-1./ch3));
      // te
      der[5*i+3] = f0*(-w*(tc-t)*sh3*(1.-S2)/(tc*te*w*SQR(ch3)));
      // Rex
      der[5*i+4] = 0.;
      } // derivatives
    } // te > 0.1 ps

  else { // te ~ 0
    jw[i] = f0*(S2*c/(ch2+s)); // (s/rad)
    if (d != 0) {
      // tc
      der[5*i+0] = f0*(-e*sh2*c*S2/(tc*SQR(s+ch2)));
      // e
      der[5*i+1] = f0*(M_PI_2*S2*s/(s+ch2));
      // S2
      der[5*i+2] = f0*((c/(ch2+s)));
      // te
      der[5*i+3] = 0.;
      // Rex
      der[5*i+4] = 0.;
      } // derivatives
    } // te ~ 0
  } // i
}
