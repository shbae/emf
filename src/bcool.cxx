/*
    eMF Copyright 2008-2009 Sung-Hun Bae

    This file is part of eMF.

    eMF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    eMF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with eMF.  If not, see <http://www.gnu.org/licenses/>
*/

#include "dataset.h"
#include "methods.h"

#include "ool_conmin.h"
#include "ool_tools_diff.h"

using namespace std;

double bc_ool (void *data)
{
  dataset *D = (dataset *)data;
  int nx = D->work_nx;
  int nv = D->work_nv;

  if (nv == 0) return -1.; // do nothing

  extern gsl_rng * rng;

  // active-set method (very fast, Hessian matrix required)
  // numeric Hessian is used
  const ool_conmin_minimizer_type *T = ool_conmin_minimizer_gencan;
  ool_conmin_gencan_parameters P;

  /*
  spectral projected gradient method (fast)
  const ool_conmin_minimizer_type *T = ool_conmin_minimizer_spg;
  ool_conmin_spg_parameters P;

  projected gradient method (slow)
  const ool_conmin_minimizer_type *T = ool_conmin_minimizer_pgrad;
  ool_conmin_pgrad_parameters P;
  */

  ool_conmin_function    F;
  ool_conmin_constraint  C;
  ool_conmin_minimizer  *M;

  // parameters
  gsl_vector *v = gsl_vector_alloc (nv);
  D->set_v (v);

  // function
  F.n	= nv;
  F.f	= &mmin_f;
  F.df	= &mmin_df;
  F.fdf	= &mmin_fdf;
  F.Hv	= &mmin_Hv;
  F.params = data;

  // constraints
  C.n	= nv;
  C.L	= gsl_vector_alloc (nv);
  C.U	= gsl_vector_alloc (nv);
  D->set_bc (C.L,C.U);

  // Starting minimizer parameters
  ool_conmin_parameters_default( T, (void*)(&P) );

  // Allocation of minimizer
  M = ool_conmin_minimizer_alloc(T, nv);

  ool_conmin_minimizer_set( M, &F, &C, v, (void*)(&P) );

  unsigned int iter = 0;
  int status = GSL_CONTINUE;

  do {
    iter++;
    status = ool_conmin_minimizer_iterate (M);
    if (status) break;

    if ( isnan (M->f)) {

#ifdef DEBUG
      clog << "warning: bc_ool rescued at ";
      for (i=0;i<nv;i++)
	clog << gsl_vector_get(M->x,i) << " ";
      clog << endl;
#endif

      D->set_v_rng (M->x, rng);
      ool_conmin_minimizer_restart (M);
      status = GSL_CONTINUE;
      }
    else
      status = ool_conmin_is_optimal (M);

    } while (status == GSL_CONTINUE && iter < D->mo.bc_OOL_stop_iter);

#ifdef DEBUG
  if(status == OOL_SUCCESS)
    printf("\nConvergence in %i iterations [status=%d]", iter,status);
  else
    printf("\nStopped with %i iterations", iter);

  printf( "\nvariables................: %6i"
	  "\nfunction evaluations.....: %6i"
	  "\ngradient evaluations.....: %6i"
	  "\nfunction value...........: % .6e"
	  "\nprojected gradient norm..: % .6e\n",
	  nv,
	  ool_conmin_minimizer_fcount( M ),
	  ool_conmin_minimizer_gcount( M ),
	  ool_conmin_minimizer_minimum( M ),
	  ool_conmin_minimizer_size( M ));

#endif

  D->store_v (M->x,NULL,M->f);

  gsl_vector_free (C.L);
  gsl_vector_free (C.U);
  gsl_vector_free (v);
  ool_conmin_minimizer_free (M);

  return M->f;
}
