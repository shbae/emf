/*
    eMF Copyright 2008-2009 Sung-Hun Bae

    This file is part of eMF.

    eMF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    eMF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with eMF.  If not, see <http://www.gnu.org/licenses/>
*/

#include "dataset.h"
#include "funclib.h"
#include "util.h"

using namespace std;

//////////////////////////////////////////////////////////////////
//
//	INPUT
//
//////////////////////////////////////////////////////////////////

void dataset::readfile (const char *filename)
{
  vector <string> c;
  string buffer;
  int resid_idx = 0;
  int vectors = 0;
  optimize_Drr = false;
  
  ifstream FILE (filename);
  while (FILE.is_open() && !FILE.eof()) 
  {
    getline(FILE,buffer);
    if (parse(buffer.c_str(),"\t ",c) != 0) 
    {
      if (c[0].find("METHOD",0) != string::npos) 
      {
	_METHOD (c);
	clog << buffer << endl;
      }
      if (c[0].find("FUNCTION",0) != string::npos) 
      {
	_FUNCTION (c);
	clog << buffer << endl;
      }
      if (c[0].find("DIFFUSION",0) != string::npos) 
      {
	optimize_Drr = true;
	_FUNCTION (c);
	clog << buffer << endl;
      }
      if (c[0].find("CONST",0) != string::npos) 
      {
	_CONST (c);
	clog << buffer << endl;
      }
      if (c[0].find("SET",0) != string::npos) 
      {
	_SET (c);
	clog << buffer << endl;
      }
      if (c[0].find("VECTOR",0) != string::npos) 
      {
	vectors = _VECTOR (c);
	clog << buffer << " (" << vectors << ")" << endl;
      }
      
      if (c[0].find("RESID",0) != string::npos)
	resid_idx = _RESID (c);
      if (c[0].find("DATA",0) != string::npos)
	__DATA(resid_idx,c);
    }
  }
  FILE.close();
}

//
// syntax: METHOD <minimizer name> [MC|trial]
//
void dataset::_METHOD (const vector <string> &c)
{
  int u = m_index (c[1]); // Rm index
  if (u < 0) 
  {
    cerr << "error: METHOD "<< c[1] <<" is undefined." << endl;
    exit(1);
  }
  if (c.size()==3) {
    if (c[1] == "RAND") mo.random_trial = atoi(c[2].c_str());
    else if (c[1] == "QRAND") mo.qrandom_trial = atoi(c[2].c_str());
    else if (c[1] == "RANDSIMPLEX") mo.rsimplex_trial = atoi(c[2].c_str());
    else Rm[u].MC = atoi(c[2].c_str());
  } 
  Qm.push_back (Rm[u]);
}

//
// syntax: FUNCTION <function name> <active set flag> [<active set flag> ... ]
// note: each active set is regarded as different function in the queue
//
void dataset::_FUNCTION (const vector <string> &c)
{
  int i,j,u,v,w;
  if (c.size() < 3) 
  {
    cerr << "error: FUNCTION syntax error\n";
    exit(1);
  }
  else if ((u=f_index(c[1])) < 0) 
  {
    cerr << "error: FUNCTION " << c[1] << " is undefined." << endl;
    exit(1);
  }
  
  v = Rf[u].p.size(); // number of parameters
  for(i=2;i<c.size();i++) 
  {
    if (c[i].length() != v) 
    {
      cerr << "error: FUNCTION " << c[1];
      cerr << " invalid bit string.\n";
      exit(1);
    }
    Qf.push_back (Rf[u]); // add functions to the queue
    w = Qf.size()-1; // index of Qf
    Qf[w].bitstr = c[i]; // keep bit string
    for (j=0;j<v;j++) // set fit flag
      Qf[w].p[j].flag = (c[i].substr(j,1) == "1" ? true : false);
  }
}

//
// syntax: CONST <constant title> Value
//
void dataset::_CONST (const vector <string> &c)
{
  extern double gamma_h,gamma_x,r_xh,csa_x,beta; // user accessible constants
  extern double gamma_ratio, fd, fd_4, fd_8, fg; // internal constants
  extern double mu0_h_8pi2; // internal constants
  if (c[1] == "gamma_h") 
    gamma_h = atof(c[2].c_str());
  else if (c[1] == "gamma_x") 
    gamma_x = atof(c[2].c_str());
  else if (c[1] == "r_xh") 
    r_xh = atof(c[2].c_str());
  else if (c[1] == "csa_x")
    csa_x = atof(c[2].c_str());
  else if (c[1] == "beta")
    beta = DEG2RAD(atof(c[2].c_str()));
  else 
  {
    cerr << "error: CONST undefined constant\n";
    exit(1);
  }
  // update internal constants
  gamma_ratio 	= gamma_x/gamma_h;
  fd 		= 1.e+3*gamma_h*gamma_x*mu0_h_8pi2/(r_xh*r_xh*r_xh);
  fd_4 		= fd*fd/4.;
  fd_8 		= fd*fd/8.;
  fg 		= (gamma_h/gamma_x)*fd*fd/4.;
}


//
// syntax: SET <function title> <parameter title> Value
// syntax: SET <function title> <parameter title> Lower Upper
// syntax: SET <function title> <parameter title> Lower Upper Grids
//
// note: more than one FUCTION should be declared before SET keyword 
//
void dataset::_SET (const vector <string> &c)
{
  int i,u,v;
  if ((u=f_index(c[1])) < 0) 
  { // u = function index of Rf repository
    cerr << "error: SET function not defined" << endl;
    exit(1);
  }
  if ((v=p_index(c[1],c[2])) < 0) 
  { // v = parameter index
    cerr << "error: SET parameter not found" << endl;
    exit(1);
  }
  for (i=0;i<Qf.size();i++) 
  {
    if (Qf[i].title == c[1]) 
    {
      switch (c.size()-3) 
      {
	case 1:
	  Qf[i].p[v].value = atof(c[3].c_str()); 
	  Qf[i].p[v].flag = false; // fix parameter
	  break;
	case 2:
	  Qf[i].p[v].lower = atof(c[3].c_str()); 
	  Qf[i].p[v].upper = atof(c[4].c_str()); 
	  break;
	case 3:
	  Qf[i].p[v].lower = atof(c[3].c_str()); 
	  Qf[i].p[v].upper = atof(c[4].c_str()); 
	  Qf[i].p[v].ngrid = atoi(c[5].c_str()); 
	  break;
	default:
	  clog << "error: SET syntax error\n";
	  exit(1);
      }
    }
  }
}

//
// syntax: VECTOR <atom name 1> <atom name 2> <pdb filename>
//
int  dataset::_VECTOR (const vector <string> &c)
{
  string atom1 = c[1];
  string atom2 = c[2];
  ifstream pdb (c[3].c_str());
  string s,name,xyz,resSeq;
  double x,y,z,l;
  int i,res,n;
  size_t pos;
  bool done;
  ATOM new_atom;
  
  if (pdb.fail()) 
  {
    cerr << "error: VECTOR cannot read " << c[3] << endl;
    exit(1);
  }
  
  while (getline(pdb,s)) 
  {
    if(s.find("ATOM") != string::npos) 
    {
      name = s.substr(12,4); 	// PDB format version 2.3
      resSeq = s.substr(22,4);// PDB format version 2.3
      xyz = s.substr(30,24); 	// PDB format version 2.3
      while ((pos=name.find(" ",0)) != string::npos)
	name.replace(pos,1,"");// trim trailing spaces
	if (name == atom1 || name == atom2) 
	{
	  sscanf(resSeq.c_str(),"%d",&res);
	  sscanf(xyz.c_str(),"%lf %lf %lf",&x,&y,&z);
	  for (done=false,i=0;i<atom.size();i++) 
	  {
	    if (atom[i].resSeq == res) 
	    {
	      atom[i].name.push_back(name);
	      atom[i].coor.push_back(x);
	      atom[i].coor.push_back(y);
	      atom[i].coor.push_back(z);
	      done = true;
	      break;
	    }
	  }
	  if (! done) 
	  {
	    atom.push_back (new_atom);
	    atom[atom.size()-1].resSeq = res;
	    atom[atom.size()-1].name.push_back(name);
	    atom[atom.size()-1].coor.push_back(x);
	    atom[atom.size()-1].coor.push_back(y);
	    atom[atom.size()-1].coor.push_back(z);
	  }
	}
    } // ATOM
  } // eof
    
  // cleanup
  n=0;
  for (i=0;i<atom.size();i++) 
  {
    if (atom[i].coor.size()==6) 
    {
      n++;
      if (atom[i].name[0] == atom2) 
      {
	x = atom[i].coor[0];
	y = atom[i].coor[1];
	z = atom[i].coor[2];
	atom[i].coor[0] = atom[i].coor[3];
	atom[i].coor[1] = atom[i].coor[4];
	atom[i].coor[2] = atom[i].coor[5];
	atom[i].coor[3] = x;
	atom[i].coor[4] = y;
	atom[i].coor[5] = z;
      }
    }
  }
  return n;
}
    
    //
    // syntax: RESID <residue number> [flag]
    //
    int dataset::_RESID  (const vector <string> &c)
    {
      RESIDUE r;
      double l;
      int i;
      r.num = atoi (c[1].c_str()); 		// get residue number
      r.flag = (c.size() == 2 ? true : (bool) atoi(c[2].c_str()) );
      for (i=0;i<Qf.size();i++) { 		// add each function in the Queue
	r.f.push_back(Qf[i]);
    } // i
    
    for (i=0;i<atom.size();i++) {		// unit bond vector
      if (atom[i].resSeq == r.num && atom[i].coor.size() == 6) {
	r.vec[0] = atom[i].coor[0] - atom[i].coor[3]; 	// x
	r.vec[1] = atom[i].coor[1] - atom[i].coor[4];	// y	
	r.vec[2] = atom[i].coor[2] - atom[i].coor[5];	// z
	l = sqrt (SQR(r.vec[0])+SQR(r.vec[1])+SQR(r.vec[2]));
	r.vec[0] /= l;	// unit bond vector
	r.vec[1] /= l;	// unit bond vector
	r.vec[2] /= l; 	// unit bond vector
      }
    }
    resid.push_back(r);			// add a residue
    return (resid.size()-1);		// return new resid index
  }
  
  
  
//
// syntax: DATA <data type(integer)> <x> <y> <dy(error)>
//
void dataset::__DATA  (const int resid_idx, const vector <string> &c)
{
  vecd data;
  // store whole columns
  for (int i=1; i<c.size(); i++)
    data.push_back(atof(c[i].c_str()));
  // interpret columns
  resid[resid_idx].c.push_back (data);
  resid[resid_idx].z.push_back (atoi(c[1].c_str()));
  resid[resid_idx].x.push_back (atof(c[2].c_str()));
  resid[resid_idx].y.push_back (atof(c[3].c_str()));
  resid[resid_idx].dy.push_back (atof(c[4].c_str()));
}

//////////////////////////////////////////////////////////////////
//
//	OUTPUT
//
//////////////////////////////////////////////////////////////////
  
void dataset::printout ()
{
  for (int r=0;r<resid.size();r++) 
  {
    cout << "RESIDUE " << resid[r].num;
    if (atom.size() > 0) 
    {
      cout << " a " << resid[r].alpha << " (deg) ";
    }
    cout << endl << endl;
    
    for (int f=0;f<resid[r].f.size();f++) 
    {
      // function/model
      if (f == resid[r].select)
	cout << setw(2) << right << ">";
      else
	cout << setw(2) << right << " ";
      cout << setw(6) << right;
      cout << resid[r].f[f].title << " ";
      cout << resid[r].f[f].bitstr << " ";
      cout << "DF= "<< resid[r].f[f].s.dof;
      cout << resetiosflags (ios_base::floatfield);
      cout << setprecision(5);
      cout << " CHISQ= " << resid[r].f[f].s.chisq; 
      cout << " BIC= " << resid[r].f[f].s.bic;
      cout << " AIC= " << resid[r].f[f].s.aic;
      cout << endl;
      
      // parameters
      vecd calcpar,calc,dyda;
      vecb b;
      calcpar.resize(0);
      b.resize(0);
      dyda.resize(0);
      for (int p=0;p<resid[r].f[f].p.size();p++) 
      {
	cout << setw(8) << " ";	// indent
	cout << setw(8) << left;
	cout << resid[r].f[f].p[p].title ;
	double value = resid[r].f[f].p[p].value;
	double error = resid[r].f[f].p[p].error;
	if (fabs(value) > 1.e-5 && fabs(value) < 1.e+5) 
	{
	  cout << fixed;
	  cout << setprecision(4);
	  cout << setw(12) << right << value;
	}
	else 
	{
	  cout << resetiosflags (ios_base::floatfield);
	  cout << setprecision(4);
	  cout << setw(12) << right << value;
	}
	if (fabs(error) > 1.e-5 && fabs(error) < 1.e+5) 
	{
	  cout << fixed;
	  cout << setprecision(4);
	  cout << setw(12) << right << error;
	}
	else 
	{
	  cout << resetiosflags (ios_base::floatfield);
	  cout << setprecision(4);
	  cout << setw(12) << right << error;
	}
	if (! resid[r].f[f].p[p].flag) 
	{
	  cout << " fixed" << endl;
	}
	else 
	{
	  cout << endl;
	} 
	calcpar.push_back (value);
	b.push_back(false);
      } // parameter
      
      // experimental vs calculated values
      assign_work (r,f);
      calc.resize (work_nx);
      (*work_fptr) (work_x,work_z,calcpar,b,this,calc,dyda);
      for (int i=0;i<work_nx;i++) 
      {
	cout << setw(8) << " ";	// indent
	cout << "ZXY";
	cout << setw(3)  << right << work_z[i];// Z
	if (fabs(work_x[i]) > 1.e-5 && fabs(work_x[i]) < 1.e+5) 
	{
	  cout << fixed;
	  cout << setprecision(4);
	}
	else
	{
	  cout << resetiosflags (ios_base::floatfield);
	  cout << setprecision(4);
	}
	cout << setw(10) << right << work_x[i];// X
	
	if (fabs(work_y[i]) > 1.e-5 && fabs(work_y[i]) < 1.e+5) 
	{
	  cout << fixed;
	  cout << setprecision(4);
	}
	else
	{
	  cout << resetiosflags (ios_base::floatfield);
	  cout << setprecision(4);
	}
	cout << setw(10) << right << work_y[i];// Y
	
	cout << setw(8) << right << "CALY";
	if (fabs(calc[i]) > 1.e-5 && fabs(calc[i]) < 1.e+5) 
	{
	  cout << fixed;
	  cout << setprecision(4);
	}
	else
	{
	  cout << resetiosflags (ios_base::floatfield);
	  cout << setprecision(4);
	}
	cout << setw(10) << right << calc[i];// CALY
	cout << setw(8) << right << "DY";
	if (fabs(work_dy[i]) > 1.e-5 && fabs(work_dy[i]) < 1.e+5) 
	{
	  cout << fixed;
	  cout << setprecision(4);
	}
	else
	{
	  cout << resetiosflags (ios_base::floatfield);
	  cout << setprecision(4);
	}
	cout << setw(10) << right << work_dy[i];// DY
	cout << endl;
      } // for each data point
      cout << endl;
    } // function
  } // residue
}
    
void dataset::print_table ()
{
  set <string> tpar;
  set <string>::iterator c;
  int r,f,p,sf;
  
  cout << "TABLE" << endl;
  
  // check relevant parameters
  for (f=0;f<Qf.size();f++) 
  {
    for (p=0;p<Qf[f].p.size();p++) 
    {
      if (Qf[f].bitstr.substr(p,1) == "1")
	tpar.insert(Qf[f].p[p].title);
    }
  }
  
  // print table header
  cout << setw(3) << " ";
  for (c=tpar.begin(); c!=tpar.end(); ++c) 
  {
    cout << setw(7) << left << *c << " ";
    cout << setw(7) << "error" << " ";
  }
  cout << endl;
  
  // print table body
  for (r=0;r<resid.size();r++) 
  {
    cout << setw(3) << resid[r].num; 
    sf = resid[r].select;
    for (c=tpar.begin(); c!=tpar.end(); ++c) 
    {
      for (p=0;p<resid[r].f[sf].p.size();p++) 
      {
	if (*c == resid[r].f[sf].p[p].title) 
	{
	  cout << fixed << setprecision(3);
	  cout << setw(7) << resid[r].f[sf].p[p].value << " ";
	  cout << setw(7) << resid[r].f[sf].p[p].error << " ";
	  break;
	} // if
      } // p
    } // c
    cout << endl;
  } // r
}
