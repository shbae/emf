/*
    eMF Copyright 2008-2009 Sung-Hun Bae

    This file is part of eMF.

    eMF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    eMF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with eMF.  If not, see <http://www.gnu.org/licenses/>
*/

#include "dataset.h"
#include "funclib.h"
#include "methods.h"
#include "util.h"

using namespace std;

//////////////////////////////////////////////////////////////////////////////
//  Constants in MKS unit:
//
//  Included in the gsl_const_mks.h
//  GSL_CONST_MKS_VACUUM_PERMEABILITY (1.25663706144e-6)  (kg m / A^2 s^2)
//  GSL_CONST_MKS_PLANCKS_CONSTANT_H (6.62606876e-34)  (kg m^2 / s)
//  GSL_CONST_MKS_PLANCKS_CONSTANT_HBAR (1.05457159642e-34) (kg m^2 / s)
//
//  mu0     : 4*M_PI*1e-7;        # N m^-2 or kg m^-1 s^-2 or T m A^-1
//  h       : 6.62606876e-34;     # J*s or kg m^2 s^-1
//  gamma_h : 26.7522e+7;         # 1H  rad T^-1 s^-1, T = kg s^-2 A^-1
//  gamma_c :  6.7283e+7;         # 13C rad T^-1 s^-1, or kg^-1 s A
//  gamma_n : -2.7126e+7;         # 15N rad T^-1 s^-1
//  r_xh    : 1.02e-10;           # 1H-15N m
//  r_xh    : 1.09e-10;           # 1H-13C m
//  csa_x   : -172e-6;            # 15N, amide
//  csa_x   : 25e-6;              # 13C, methine alpha-carbon
//  d       : gamma_h*gamma_x*r_xh^-3*mu0*h/(8*M_PI^2);
//  c       : wx*csa/sqrt(3.0);
//
//  NOTE: for unit analysis, A(ampere) ~ m
//  CAUTION: internally, correlation times are treated in ns unit
//        	and angular frequency is treated in 1e+9 rad/sec.
//
//////////////////////////////////////////////////////////////////////////////


// default physical constants for relaxation
double  gamma_h     	= 26.7522;	  // e+7
double  gamma_c     	=  6.7283;	  // e+7
double  gamma_n     	= -2.7126;	  // e+7
double  gamma_x     	= gamma_n;
double  r_xh	      	= 1.02;		  // e-10
double  csa_x	      	= -160;		  // e-6
double	beta		= DEG2RAD (18.);
double  mu0_h_8pi2  	= 1.0545887;	  // unit e-41
double  gamma_ratio 	= gamma_x/gamma_h;
double  fd	      	= 1.e+3*gamma_h*gamma_x*mu0_h_8pi2/(r_xh*r_xh*r_xh);
double  fd_4	      	= fd*fd/4.;
double  fd_8	      	= fd*fd/8.;
double  fg	      	= (gamma_h/gamma_x)*fd*fd/4.;


gsl_rng * rng; // global random number generator

int main(int argc, char *argv[])
{

dataset D;
Load_Functions (D);
Load_Methods (D);

if (argc != 2) 
	{
	// signature
	cout << endl;
	cout << "\t\t\t\t\t\t\tSung-Hun Bae Oct. 2009\n";
	cout << "\teMF version 2.0\n";
	cout << "\tUsage: eMF [data file]\n\n";
	exit(1);
	}

D.readfile (argv[1]);

// checking elapsed time
time_t time_start,time_finish;
time(&time_start);

// set up random number generation
unsigned long int seed = time(0);
extern gsl_rng * rng;
const gsl_rng_type * T = gsl_rng_ranlux389;
rng = gsl_rng_alloc (T);
gsl_rng_set (rng, seed);


//////////////////////////////////////////////////////////////////////////////
//
// Optimize Rotational Diffusion Tensor, Drr
//
//////////////////////////////////////////////////////////////////////////////

if (D.optimize_Drr) optimize_Drr (D);

//////////////////////////////////////////////////////////////////////////////
//
// Fit function(s) / model(s)
//
//////////////////////////////////////////////////////////////////////////////

clog << "\nFitting Residue(s) ......\n";
for (int r=0;r<D.resid.size();r++) {
	clog << "\tResid " << D.resid[r].num << endl;
	for (int f=0;f<D.resid[r].f.size();f++) {
		// assign residue and function for fitting
		D.assign_work (r,f);
		// fitting procedure
		double chisq = 0.0;
		for (int m=0;m<D.Qm.size();m++) {
			chisq = (D.Qm[m].ptr) (&D);
			} // methods
		// store fitted parameters
		for (int p=0;p<D.resid[r].f[f].p.size();p++) {
			D.resid[r].f[f].p[p].value = D.work_p[p].value;
			D.resid[r].f[f].p[p].error = D.work_p[p].error;
			}
		// store fitting statistics
		D.resid[r].f[f].s.dof = D.work_nx - D.work_nv;
		D.resid[r].f[f].s.chisq = chisq;	// last chisq
		D.resid[r].f[f].s.bic = chisq + 
			D.work_nv*log((double)D.work_nx);
		D.resid[r].f[f].s.aic = chisq + D.work_nv*2;
		// log
		clog << "\t\t";
		clog << D.resid[r].f[f].title;
		clog << " " << D.resid[r].f[f].bitstr;
		clog << " DF " << D.resid[r].f[f].s.dof;
		clog << " X2 " << chisq;
		clog << endl;
		} // functions
	} // residues


//////////////////////////////////////////////////////////////////////////////
//
// select best function using Bayesian Information Criteria
//
//////////////////////////////////////////////////////////////////////////////

D.select (BIC); 


//////////////////////////////////////////////////////////////////////////////
//
// Monte Carlo simulations for fitting error estimation
//
//////////////////////////////////////////////////////////////////////////////

clog << "Monte Carlo Simulation ......" << endl;
for (int r=0;r<D.resid.size();r++) {
	clog << " Resid " << D.resid[r].num << endl;
	int f = D.resid[r].select;
	D.assign_work (r, f);
	for (int m=0;m<D.Qm.size();m++) {
		vector < vector < PARAMETER > > par;
		vector <double> x2;
		int c = 0;
		while (c < D.Qm[m].MC) {
			// generate synthetic data set using 
			// experimental error
			D.synthetic (rng);
			// fit synthetic data set
			double chisq = (D.Qm[m].ptr) (&D);
			// record fitting results
			if (!isnan(chisq)) {
				c++;
				x2.push_back (chisq);
				par.push_back (D.work_p);
				}
			} 
		if (c > 1) D.store_mc (c, x2, par);
		} // methods
	// store error
	for (int p=0;p<D.resid[r].f[f].p.size();p++)
		D.resid[r].f[f].p[p].error = D.work_p[p].error;

	} // residues


//////////////////////////////////////////////////////////////////////////////
//
// Display Fitting Results
//
//////////////////////////////////////////////////////////////////////////////

D.printout ();


//////////////////////////////////////////////////////////////////////////////
// exit
//////////////////////////////////////////////////////////////////////////////

gsl_rng_free (rng);
time (&time_finish);
int hh=0,mm=0,ss=0,elapsed=time_finish-time_start;
hh = (int)floor((double)(elapsed/3600));
mm = (int)floor((double)((elapsed/60)-(hh*60)));
ss = (elapsed - (hh*3600)-(mm*60));
cout << endl;
cout << "## started        " << ctime(&time_start);
cout << "## finished       " << ctime(&time_finish);
cout << "## time elapsed   " << setfill ('0') << setw(2) << hh;
cout << ":" << setw(2) << mm;
cout << ":" << setw(2) << ss << endl;
}
