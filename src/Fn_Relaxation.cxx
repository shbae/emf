/*
    eMF Copyright 2008-2009 Sung-Hun Bae

    This file is part of eMF.

    eMF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    eMF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with eMF.  If not, see <http://www.gnu.org/licenses/>
*/

//////////////////////////////////////////////////////////////////
//
// 	Relaxation Rates
//	- call specified spectral density function
//
//////////////////////////////////////////////////////////////////

#include "dataset.h"
#include "funclib.h"
#include "util.h"

using namespace std;

void relaxation (void (*Jw) _LF_,
  const vecd &x, const veci &z, const vecd &p, const vecb &b,
  const void *data, vecd &y, vecd &dyda) 
{
  extern double gamma_ratio,fd,fd_4,fd_8,fg,csa_x;
  dataset * D = (dataset *) data;
  double wh,wx,fc,fc_6,R1i,dR1,fcd;
  int nx = x.size();
  int np = p.size();
  int nd = dyda.size();
  vecd w,jw,der;
  
  w.resize(5); 
  jw.resize(5);
  der.resize(5*np); // number of w freq. x np, dJw_i/dp_j
  
  for (int i=0;i<nx;i++) 
  {  
    // field dependent constants
    wh = x[i]*2.*M_PI*1.e-3; // 1e+9 (rad/s)
    wx = wh*gamma_ratio; // 1e+9 (rad/s)
    fc = 1.e+6*wx*wx*csa_x*csa_x/3.;
    fc_6 = fc/6.;
    fcd = -fd*1.e+3*wx*csa_x/6.;
    // for auto-relaxation
    w[0] = 0.0; // also for cross-correlated relaxation
    w[1] = wx;	// also for cross-correlated relaxation
    w[2] = wh-wx;
    w[3] = wh;
    w[4] = wh+wx;
    
    // call spectral density function
    Jw (w, p, b, data, jw, der); // (s/rad)
   
    // R1 (1/s)
    if (z[i] == 0) 
    {
      y[i] = fd_4*(jw[2]+3.*jw[1]+6.*jw[4])+fc*jw[1];
      for (int j=0; nd>0 && j<np; j++) 
      {
	dyda[i*np+j] = fd_4*(der[2*np+j]+3.*der[1*np+j] +6.*der[4*np+j]) +fc*der[1*np+j];
      }
    }
    
    // R2 (1/s)
    if (z[i] == 1) 
    {
      y[i] = fd_8*(4.*jw[0]+jw[2]+3.*jw[1]+6.*jw[3]+6.*jw[4]) +fc_6*(4.*jw[0]+3.*jw[1]);
      for (int j=0; nd>0 && j<np; j++) 
      {
	dyda[i*np+j] = fd_8*(4.*der[0*np+j]+der[2*np+j] +3.*der[1*np+j]+6.*der[3*np+j]+ 6.*der[4*np+j]) 
	+fc_6*(4.*der[0*np+j]+3.*der[1*np+j]);
      }
    }
    //?? exclude high frequency terms
    if (z[i] == 9) 
    {
      y[i] = fd_8*(4.*jw[0]+3.*jw[1]) +fc_6*(4.*jw[0]+3.*jw[1]);
      for (int j=0; nd>0 && j<np; j++) 
      {
	dyda[i*np+j] = fd_8*(4.*der[0*np+j] +3.*der[1*np+j]) +fc_6*(4.*der[0*np+j]+3.*der[1*np+j]);
      }
    }
    
    // heteronuclear NOE
    if (z[i] == 2) 
    {
      R1i= 1./(fd_4*(jw[2]+3.*jw[1]+6.*jw[4])+fc*jw[1]);
      y[i] = 1.+fg*(6.*jw[4]-jw[2])*R1i;
      for (int j=0; nd>0 && j<np; j++) 
      {
	dR1 = fd_4*(der[2*np+j]+3.*der[1*np+j] +6.*der[4*np+j]) +fc*der[1*np+j];
	dyda[i*np+j] = fg*((6.*der[4*np+j]-der[2*np+j])*R1i -(6.*jw[4]-jw[2])*SQR(R1i)*dR1);
      }
    }
    
    // Eta_XY - transverse DD-CSA cross correlation rate
    if (z[i] == 3) 
    {
      y[i] = fcd*(4.*jw[0]+3.*jw[1]);
      for (int j=0; nd>0 && j<np; j++) 
      {
	dyda[i*np+j] = fcd*(4.*der[0*np+j]+3.*der[1*np+j]);
      }
    }
    
    // Eta_Z - longitudinal DD-CSA cross correlation rate
    if (z[i] == 4) 
    {
      y[i] = 6.*fcd*(jw[1]);
      for (int j=0; nd>0 && j<np; j++) 
      {
	dyda[i*np+j] = 6.*fcd*(der[1*np+j]);
      }
    }
  } // i : number of data points
}
