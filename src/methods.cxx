/*
    eMF Copyright 2008-2009 Sung-Hun Bae

    This file is part of eMF.

    eMF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    eMF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with eMF.  If not, see <http://www.gnu.org/licenses/>
*/

#include "dataset.h"
#include "methods.h"
#include "ool_conmin.h"
#include "ool_tools_diff.h"

using namespace std;

void Load_Methods (dataset &D)
{
  int idx;
  idx = D.define_method ("GRID",&grid);
  idx = D.define_method ("QRAND",&qrand);
  idx = D.define_method ("RAND",&rand);
  idx = D.define_method ("RANDSIMPLEX",&randsimplex);
  idx = D.define_method ("LEVMAR",&levmar);
  idx = D.define_method ("LEVMAR-ML",&levmarML);
  idx = D.define_method ("LEVMAR-ML-BC",&levmarML_bc);
  idx = D.define_method ("BCOOL",&bc_ool);
  idx = D.define_method ("CONJGR",&conjgr);
  idx = D.define_method ("SIMPLEX",&simplex);
  idx = D.define_method ("SA",&sa);

  D.mo.qrandom_trial		= 500;
  D.mo.random_trial		= 500;
  D.mo.rsimplex_trial		= 50;
  D.mo.simplex_stop_size	= 1.e-4;
  D.mo.simplex_stop_iter	= 5000;
  D.mo.levmar_stop_iter		= 1000000;
  D.mo.levmar_stop_grad		= 1.e-12;
  D.mo.levmar_stop_delta	= 1.e-12;
  D.mo.levmar_ML_stop_iter	= 1000000;
  D.mo.levmar_ML_ini_mu		= 1.e-3;  // initial \mu
  D.mo.levmar_ML_stop_Je	= 1.e-12; // stop threshold for ||J^T e||_inf
  D.mo.levmar_ML_stop_dp2	= 1.e-12; // stop threshold for ||Dp||_2
  D.mo.levmar_ML_stop_e2	= 1.e-15; // stop threshold for ||e||_2
  D.mo.levmar_ML_stop_jac	= 1.e-6;  // for finite difference jacobian
  D.mo.bc_OOL_stop_iter		= 1000000;
  /*
  0: // Fletcher-Reeves
  1: // Polak-Ribiere
  2: // Broyden-Fletcher-Goldfarb-Shanno (BFGS)
  3: // Broyden-Fletcher-Goldfarb-Shanno (BFGS2)
  */
  D.mo.conjgr_algorithm		= 0;	// algorithm 0-3
  D.mo.conjgr_step_size		= 0.1;
  D.mo.conjgr_ftol		= 1.e-9;// tolerance
  D.mo.conjgr_stop_iter		= 1e+6;
  D.mo.conjgr_stop_grad		= 1.e-9;
  D.mo.sa_start			= 300.;
  D.mo.sa_damping		= 1.001;
  D.mo.sa_stop			= 100.;
  D.mo.sa_iter			= 1000;
}

//////////////////////////////////////////////////////////////////
//
// For Grid Search, Simplex, and Simulated Annealing (GSL)
//
//////////////////////////////////////////////////////////////////

double f_chisq (const gsl_vector *v, void *data)
{
  dataset * D = (dataset *) data;
  vecd p,f,dyda;
  vecb b;

  D->set_pb (p, b, v);

  f.resize(D->work_nx);
  dyda.resize(0);

  (*D->work_fptr) (D->work_x, D->work_z, p, b, data, f, dyda);

  double chisq = 0.0;
  for(int i=0;i<D->work_nx;i++) {
    chisq += pow((f[i]-D->work_y[i])/(D->work_dy[i]),2.);
    }
  return chisq;
}

//////////////////////////////////////////////////////////////////
//
// functions for multidimensional minimization (GSL & OOL)
//
//////////////////////////////////////////////////////////////////


//
// function value = SUM [(f(p)-y)/dy]^2
//
double mmin_f (const gsl_vector *v, void *data)
{
  dataset * D = (dataset *) data;
  vecd p,f,dyda;
  vecb b;

  D->set_pb (p, b, v);
  
  f.resize(D->work_nx);
  dyda.resize(0);

  (*D->work_fptr)(D->work_x,D->work_z,p,b,data,f,dyda);

  double chisq = 0.0;
  for(int i=0;i<D->work_nx;i++) {
    chisq += pow((f[i]-D->work_y[i])/(D->work_dy[i]),2.);
    }
  return chisq;
}

//
// gradient at p = SUM 2*[(f(p)-y)/dy]*(df(p)/dp)
//
void mmin_df (const gsl_vector *v, void *data, gsl_vector *G)
{
  dataset * D = (dataset *) data;
  vecd p,f,dyda,sum_dyda;
  vecb b;

  D->set_pb (p, b, v);

  gsl_vector_set_all (G,0.0);
  sum_dyda.assign (D->work_np, 0.0);

  f.resize(D->work_nx);
  dyda.resize(D->work_nx*D->work_np);

  (*D->work_fptr)(D->work_x,D->work_z,p,b,data,f,dyda);

  for (int i=0;i<D->work_nx;i++) {
    double chi = (f[i]-D->work_y[i])/D->work_dy[i];
    for (int j=0;j<D->work_np;j++)
      sum_dyda[j] += 2. * chi * dyda[i*D->work_np+j];
    }
    D->set_grad (G, sum_dyda);
}


//
// function value and gradient
//
void mmin_fdf (const gsl_vector *v, void *data, double *fret, gsl_vector *G)
{
  dataset * D = (dataset *) data;
  vecd p,f,dyda,sum_dyda;
  vecb b;

  D->set_pb (p, b, v);

  gsl_vector_set_all (G,0.0);
  sum_dyda.assign (D->work_np, 0.0);

  f.resize(D->work_nx);
  dyda.resize(D->work_nx*D->work_np);

  (*D->work_fptr)(D->work_x,D->work_z,p,b,data,f,dyda);

  *fret = 0.0;
  for(int i=0;i<D->work_nx;i++) {
    double chi = (f[i]-D->work_y[i])/D->work_dy[i];
    *fret += chi*chi;
    for (int j=0;j<D->work_np;j++)
      sum_dyda[j] += 2.*chi*dyda[i*D->work_np+j];
    }
  D->set_grad (G, sum_dyda);
}

//
// Hessian (numerical)
//
void mmin_Hv (const gsl_vector *v,void *data, 
  const gsl_vector *V,gsl_vector *Hv)
{
  ool_diff_Hv_accel *a = ool_diff_Hv_accel_alloc (v->size);
  ool_diff_Hv (a, &mmin_df, v, data, V, Hv, 1.e-5);
  ool_diff_Hv_accel_free (a);
}

//////////////////////////////////////////////////////////////////
//
// functions for Levenberg-Marquardt (GSL)
//
//////////////////////////////////////////////////////////////////

int lm_f (const gsl_vector *v, void *data, gsl_vector *c)
{
  dataset * D = (dataset *) data;
  vecd p,f,dyda;
  vecb b;

  D->set_pb (p, b, v);
  f.resize(D->work_nx);
  dyda.resize(0);

  (*D->work_fptr)(D->work_x,D->work_z,p,b,data,f,dyda);
  
  for(int i=0;i<D->work_nx;i++) {
    gsl_vector_set (c,i,(f[i]-D->work_y[i])/D->work_dy[i]); // chi
    }

  return GSL_SUCCESS;
}


int lm_df (const gsl_vector *v, void *data, gsl_matrix *J)
{
  dataset * D = (dataset *) data;
  vecd p,f,dyda;
  vecb b;

  D->set_pb (p, b, v);
  f.resize(D->work_nx);
  dyda.resize(D->work_nx*D->work_np);

  (*D->work_fptr)(D->work_x,D->work_z,p,b,data,f,dyda);

  D->set_jacob (J,dyda);

  return GSL_SUCCESS;
}


int lm_fdf (const gsl_vector *v, void *data, gsl_vector *c,gsl_matrix *J)
{
  dataset * D = (dataset *) data;
  int nx = c->size;
  vecd p,f,dyda;
  vecb b;

  D->set_pb (p, b, v);
  f.resize(D->work_nx);
  dyda.resize(D->work_nx*D->work_np);

  (*D->work_fptr)(D->work_x,D->work_z,p,b,data,f,dyda);

  for(int i=0;i<D->work_nx;i++) {
    gsl_vector_set(c,i,(f[i]-D->work_y[i])/D->work_dy[i]);
    }

  D->set_jacob (J,dyda);

  return GSL_SUCCESS;
}

//////////////////////////////////////////////////////////////////
//
// functions for Levenberg-Marquardt (Manolis Lourakis)
//
//////////////////////////////////////////////////////////////////

// function evaluation
void lm_ML_f (double *v, double *fret, int nv, int nx, void *data)
{
  dataset * D = (dataset *) data;
  vecd p,f,dyda;
  vecb b;

  gsl_vector_const_view vv = gsl_vector_const_view_array (v, nv);

  D->set_pb (p, b, &vv.vector);
  f.resize(nx);
  dyda.resize(0);

  (*D->work_fptr) (D->work_x,D->work_z,p,b,data,f,dyda);
  
  for(int i=0;i<nx;i++) {
    fret[i] = f[i];
    }
}

// jacobian
void lm_ML_df (double *v, double *J, int nv, int nx, void *data)
{
  dataset * D = (dataset *) data;
  vecd p,f,dyda;
  vecb b;

  gsl_vector_const_view vv = gsl_vector_const_view_array (v, nv);
  gsl_matrix_view vJ = gsl_matrix_view_array (J, nx, nv);

  D->set_pb (p, b, &vv.vector);

  f.resize(nx);
  dyda.resize(nx*D->work_np);

  (*D->work_fptr)(D->work_x,D->work_z,p,b,data,f,dyda);

  D->set_jacob (&vJ.matrix, dyda);
}
