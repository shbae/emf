/*
    eMF Copyright 2008-2009 Sung-Hun Bae

    This file is part of eMF.

    eMF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    eMF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with eMF.  If not, see <http://www.gnu.org/licenses/>
*/

#include "dataset.h"
#include "funclib.h"
#include "util.h"

using namespace std;

//////////////////////////////////////////////////////////////////
// function 
//////////////////////////////////////////////////////////////////

int dataset::f_index (const fptr f)
{
  int i,idx=-1;
  for (i=0;i<Rf.size();i++) 
  {
    if (f == Rf[i].ptr) 
    {
      idx = i;
      break;
    }
  }
  return idx;
}

int dataset::f_index (const string s)
{
  int i,idx=-1;
  for (i=0;i<Rf.size();i++) 
  {
    if (s == Rf[i].title) 
    {
      idx = i;
      break;
    }
  }
  return idx;
}

int dataset::define_function (const string s, const fptr f)
{
  FUNCTION g;
  g.title = s;
  g.ptr = f;
  if (f_index(s) < 0) 
  {
    Rf.push_back (g);
  }
  else 	
  {
    std::cerr << "error: multiple definition of ";
    std::cerr << s << endl;
    exit(1);
  }
  return (Rf.size()-1);
}

//////////////////////////////////////////////////////////////////
// method 
//////////////////////////////////////////////////////////////////

int dataset::define_method (const string s, const mptr m)
{
  METHOD h = {s,m,0};
  Rm.push_back (h);
  return (Rm.size()-1);
}

int dataset::m_index (const string s)
{
  int i,idx=-1;
  for (i=0;i<Rm.size();i++) 
  {
    if (s == Rm[i].title) 
    {
      idx = i;
      break;
    }
  }
  return idx;
}

// return number of grid points of (i)th element of variable vector
int dataset::get_grid_num (int i)
{
  int u,v;
  for (u=0,v=0;u<work_np;u++) 
  {
    if (work_p[u].flag) 
    {
      if (v == i) 
	break;
      else 
	v++;
    }
  }
  return work_p[u].ngrid;
}

// return (j)th grid point of (i)th element of variable vector
double dataset::get_grid_val (int i, int j)
{
  int n,u,v;
  double min,max;
  for (u=0,v=0;u<work_np;u++) 
  {
    if (work_p[u].flag) 
    {
      if (v == i) 
	break;
      else 
	v++;
    }
  }
  n = work_p[u].ngrid;
  min = work_p[u].lower;
  max = work_p[u].upper;
  return (min+(j+0.5)*(max-min)/(n));
}

// set box constraints L and U
void dataset::set_bc (gsl_vector *L, gsl_vector *U)
{
  int k=0;
  for (int j=0; j<work_np; j++) 
  {
    if (work_p[j].flag) 
    {
      gsl_vector_set (L,k,work_p[j].lower);
      gsl_vector_set (U,k,work_p[j].upper);
      k++;
    }
  }
}

// set parameter vector <- work_p[#].value
void dataset::set_pb (vecd &p, vecb &b, const gsl_vector *v)
{
  p.resize(0);
  b.resize(0);
  int k=0;
  for (int j=0; j<work_np; j++) 
  {
    if (work_p[j].flag) 
    {
      p.push_back(gsl_vector_get(v,k));
      b.push_back(true);
      k++;
    }
    else
    {
      p.push_back(work_p[j].value);
      b.push_back(false);
    }
  }
} 

/*
 * Jacobian matrix J(i,j) = df_i/dp_j
 * where f_i = (Y_i-y_i)/sigma[i],
 * and the p_j are the fitted parameters
 * df_i/dp_j = -dy_i/sigma[i]
 *
 * dyda(i,j) = df_i/dp_j, p_j are all parameters
 * set jacobian matrix J using derivative matrix dyda
 */
void dataset::set_jacob (gsl_matrix *J, const vecd &dyda)
{
  for (int i=0; i<work_nx; i++)
  {
    int k=0;
    for (int j=0; j<work_np; j++) 
    {
      if (work_p[j].flag) 
      {
        gsl_matrix_set(J,i,k,-dyda[i*work_np+j]/work_dy[i]);
        k++;
      }
    }
  }
} 

// set GSL variable vector <- work_p[#].value
void dataset::set_v (gsl_vector *v) 
{ 
  int k=0;
  for (int j=0; j<work_np; j++)
    if (work_p[j].flag) 
    {
      gsl_vector_set (v, k, work_p[j].value);
      k++;
    }
}

// set GSL gradient vector <- v
void dataset::set_grad (gsl_vector *g, const vecd & v)
{
  int i,j;
  for (i=0,j=0;i<work_np;i++)
    if (work_p[i].flag)
    {
      gsl_vector_set (g,j,v[i]);
      j++;
    }
}

// set GSL variable vector using quasi-random number generation [0,1)
void dataset::set_v_qrng (gsl_vector *v, gsl_qrng *qrng) 
{
  int i,j;
  double max,min,q[v->size];
  gsl_qrng_get (qrng, q);
  for (i=0,j=0;i<work_np;i++) 
  {
    if (work_p[i].flag) 
    {
      max = work_p[i].upper;
      min = work_p[i].lower;
      gsl_vector_set (v,j, min+(max-min)*q[j]);
      j++;
    }
  }
}

// set GSL variable vector using random number generation [min,max)
void dataset::set_v_rng (gsl_vector *v, gsl_rng * rng) 
{
  double max,min;
  int i,j;
  for (i=0,j=0;i<work_np;i++) 
  {
    if (work_p[i].flag) 
    {
      max = work_p[i].upper;
      min = work_p[i].lower;
      gsl_vector_set (v,j, min+(max-min)*gsl_rng_uniform(rng)); 
      j++;
      // [0,1)
    }
  }
}


//////////////////////////////////////////////////////////////////
// parameter 
//////////////////////////////////////////////////////////////////

void dataset::define_parameter (const int idx,const string title,
				const double lower,const double upper,const double value,
				const int ngrid,const double tol) 
{ 
  PARAMETER p;
  p.title = title;
  p.lower = lower;
  p.upper = upper;
  p.value = value;
  p.ngrid = ngrid;
  p.tol = tol;
  p.error = 0.;
  if (fabs(upper-lower) < tol) {
    p.value = (lower+upper)/2.;
    p.flag = false;
  }
  else	{
    p.flag = true;
  }
  Rf[idx].p.push_back(p);
}

// return p index
int dataset::p_index (const string sf, const string sp)
{
  int u,v,idx=-1;
  for (u=f_index(sf), v=0;v<Rf[u].p.size(); v++) {
    if (sp == Rf[u].p[v].title) {
      idx = v;
      break;
    }
  }
  return idx;
}

// return minimum and maximum of x values
void dataset::get_x_range (double &min, double &max) 
{ 
  vecd sorted = resid[work_resid].x;
  vecd::iterator it;
  sort (sorted.begin(), sorted.end());
  min = *(it = sorted.begin());
  max = *(it = sorted.end()-1);
}


//////////////////////////////////////////////////////////////////
// work assignment
//////////////////////////////////////////////////////////////////

void dataset::assign_work (int r, int f)
{
  int i;
  work_resid 	= r;
  work_func	= f;
  work_fptr 	= resid[r].f[f].ptr;
  work_nx 	= resid[r].x.size();
  work_np 	= resid[r].f[f].p.size();
  work_synth	= false;
  
  // data
  work_x 	= resid[r].x;
  work_y 	= resid[r].y;
  work_z 	= resid[r].z;
  work_dy = resid[r].dy;
  
  // parameters
  work_p.resize(0);
  work_v.resize(0);
  for (i=0;i<work_np;i++) {
    work_p.push_back (resid[r].f[f].p[i]);
    if (resid[r].f[f].p[i].flag)
      work_v.push_back (resid[r].f[f].p[i]);
  }
  work_nv = work_v.size();
}

// generate synthetic data
void dataset::synthetic (gsl_rng *rng)
{
  work_synth = true;
  for (int i=0;i<work_nx;i++) {
    work_y[i] = resid[work_resid].y[i] + 
    gsl_ran_gaussian(rng, resid[work_resid].dy[i]);
  }
}


//////////////////////////////////////////////////////////////////
// model selection
//////////////////////////////////////////////////////////////////

// 1. eliminate unrealistic models
// 2. select the best model by criteria: BIC,AIC,AICc
void dataset::select (criterion by)
{
  int best_fn,r,f;
  for (r=0;r<resid.size();r++) {
    best_fn = -1;
    if (by == BIC) {
      for (f=0;f<resid[r].f.size();f++) {
	if (inBound(resid[r].f[f])) {
	  if (best_fn == -1) 
	    best_fn = f;
	  else if (resid[r].f[f].s.bic < resid[r].f[best_fn].s.bic) 
	    best_fn = f;
	}
      }
    }
    if (by == AIC) {
      for (f=0;f<resid[r].f.size();f++) {
	if (inBound(resid[r].f[f])) {
	  if (best_fn == -1) 
	    best_fn = f;
	  else if (resid[r].f[f].s.aic < resid[r].f[best_fn].s.aic) 
	    best_fn = f;
	}
      }
    }
    resid[r].select = best_fn;
  } // r
}

// return false if any one of parameters is out of bound
bool dataset::inBound (const FUNCTION &F)
{
  for (int p=0; p<F.p.size(); p++)
    if (F.p[p].value < F.p[p].lower || F.p[p].value > F.p[p].upper)
      return false;
    return true;
}

//////////////////////////////////////////////////////////////////
// recording results
//////////////////////////////////////////////////////////////////

//
// store variable vector -> work_p[#].value
// store errors from covariance matrix -> work_p[#].error
// 
void dataset::store_v (const gsl_vector *v, const gsl_matrix *covar, const double &work_chisq) 
{
  #ifdef INCREASE_COVAR_ERROR
  // if the chisq shows a poor fit (i.e. chisq/dof >> 1) then 
  // the error estimates obtained from the covariance matrix will be too small.
  // as a common way of increasing the errors for a poor fit, 
  // the error estimates are multiplied by \sqrt{chisq/dof}.
  double fac = ((work_nx-work_nv) == 0 ? 1 : GSL_MAX_DBL (1,sqrt(work_chisq/(work_nx-work_nv))));
  #endif
  int k=0; 
  for (int j=0; j<work_p.size(); j++) 
  {
    if (work_p[j].flag) 
    {
      work_p[j].value = gsl_vector_get(v,k);
      if (covar != NULL)
      {
	#ifdef INCREASE_COVAR_ERROR
	work_p[j].error = fac*sqrt(gsl_matrix_get(covar,k,k));
	#else
	work_p[j].error = sqrt(gsl_matrix_get(covar,k,k));
	#endif
      }
      k++;
    }
  }
}
  
  
// store chisq and fitted parameters 
// value = mean 
// error = standard deviation 
// discard 10% extreme values at maximum and minimum
void dataset::store_mc (
  const int c, const vector <double> &x2, 
  const vector <vector <PARAMETER> > &par)
{
  int i,j,n;
  vecd sample;
  double s,ep,var,mean;
  for (i=0;i<work_np;i++) 
  {
    sample.resize(0);
    for (j=0;j<c;j++)
      sample.push_back (par[j][i].value);
    sort (sample.begin(), sample.end());
    s=0.0;
    n=0;
    for (j=(int)(c*0.05);j<(int)(c*0.95);j++) 
    {
      s += sample[j];
      n++;
    }
    mean = s/n;
    ep=var=0.0;
    for (j=(int)(c*0.05);j<(int)(c*0.95);j++) 
    {
      s = sample[j] - mean;
      ep += s;
      var += s*s;
    }
    var = (var-ep*ep/n)/n;
    work_p[i].mean = mean;
    work_p[i].error = sqrt(var);
  }
}

