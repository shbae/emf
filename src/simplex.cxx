/*
    eMF Copyright 2009 Sung-Hun Bae

    This file is part of eMF.

    eMF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    eMF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with eMF.  If not, see <http://www.gnu.org/licenses/>
*/

#include "dataset.h"
#include "funclib.h"
#include "methods.h"
#include "util.h"

using namespace std;

//
// random simplex search
//
double randsimplex (void *data)
{
  dataset *D = (dataset *)data;
  if (D->work_nv == 0) return -1.;

  double x2, chisq= 1.e+25;
  extern gsl_rng * rng;

  gsl_vector *v	= gsl_vector_alloc (D->work_nv);
  gsl_vector *v_best = gsl_vector_alloc (D->work_nv);

  for (int i=0;i<D->mo.rsimplex_trial;i++) {
	D->set_v_rng (v,rng);
	x2 = simplex (data);
	if (!isnan(x2) && x2 < chisq) {
		chisq = x2;
		gsl_vector_memcpy (v_best, v);
		}
	}

  D->store_v (v_best, NULL, chisq);

  gsl_vector_free (v);
  gsl_vector_free (v_best);
  return chisq;
}

//
// simplex
//

double simplex (void *data) 
{
  dataset *D = (dataset *)data;
  unsigned int iter = 0;
  int status;
  double size;

  const gsl_multimin_fminimizer_type *T = gsl_multimin_fminimizer_nmsimplex;
  gsl_multimin_fminimizer *s = NULL;
  gsl_vector *ss, *v;
  gsl_multimin_function m;

  /* Starting point */
  int nv = D->work_nv;
  v = gsl_vector_alloc (nv);
  D->set_v (v);

  /* Set initial step sizes to 0.1 */
  ss = gsl_vector_alloc (nv);
  gsl_vector_set_all (ss, 0.1);

  /* Initialize method and iterate */
  m.n = nv;
  m.f = &f_chisq;
  m.params = data;

  s = gsl_multimin_fminimizer_alloc (T, nv);
  gsl_multimin_fminimizer_set (s, &m, v, ss);

  do {
    iter++;
    status = gsl_multimin_fminimizer_iterate(s);
    if (status) break;
    size = gsl_multimin_fminimizer_size (s);
    status = gsl_multimin_test_size (size, D->mo.simplex_stop_size);
    if (status == GSL_SUCCESS) { /* reserved */ }
    } while (status == GSL_CONTINUE && iter < D->mo.simplex_stop_iter);

  D->store_v (s->x, NULL, s->fval);

  gsl_vector_free(v);
  gsl_vector_free(ss);
  gsl_multimin_fminimizer_free (s);

  return s->fval;
}
