/*
    eMF Copyright 2008-2009 Sung-Hun Bae

    This file is part of eMF.

    eMF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    eMF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with eMF.  If not, see <http://www.gnu.org/licenses/>
*/

/*

  Local Anisotropic Model II

  Woessner, D.E. (1962) J. Chem. Phys. 37, 647-654.
  Halle, B. & Wennerstrom, H. (1981) J. Chem. Phys. 75, 1928-1943.
  Barbato, G. et al. (1992) Biochemistry 31, 5269-5278.

*/

#include "dataset.h"
#include "funclib.h"
#include "util.h"

using namespace std;

void Local_XSYM_1 (const vecd &x,const veci &z, const vecd &p, const vecb &b,
	const void *data, vecd &y, vecd &dyda)
{
dataset *D = (dataset *)data;
double x_min,x_max;

relaxation (&Jw_Local_XSYM_1, x, z, p, b, data, y, dyda);

// Quadratic Scaling of Rex
D->get_x_range (x_min,x_max);
int np = p.size();
for (int i=0;i<x.size();i++) {
	if (z[i] == 1) {
		y[i] = y[i] + p[5]*SQR(x[i]/x_min); // R2 = R20 + Rex
		if (dyda.size() > 0)
			dyda[np*i+5] = SQR(x[i]/x_min);
		}
	}
}

void Local_XSYM_2 (const vecd &x,const veci &z, const vecd &p, const vecb &b,
	const void *data, vecd &y, vecd &dyda)
{
  dataset *D = (dataset *)data;
  double x_min,x_max;

  relaxation (&Jw_Local_XSYM_2, x, z, p, b, data, y, dyda);

  // Quadratic Scaling of Rex
  D->get_x_range (x_min,x_max);
  int np = p.size();
  for (int i=0;i<x.size();i++) {
    if (z[i] == 1) {
      y[i] = y[i] + p[5]*SQR(x[i]/x_min); // R2 = R20 + Rex
      if (dyda.size() > 0)
        dyda[np*i+5] = SQR(x[i]/x_min);
      }
    }
}

//
// Local Axially Symmetric: Jw (w, Dpar, Dper, cos2, S2, te, Rex)
//
void Jw_Local_XSYM_1 (const vecd &w, const vecd &p, const vecb &b,
	const void *data, vecd &jw, vecd &der)
{
  const double fac = 0.4e-9; // 2/5
  double Dpar,Dper,cos2,t,S2,te,Rex;
  double tc[3],A[3],dtcdDper[3],dtcdDpar[3],dAdcos2[3];
  double p0,p1,p2,s0,s1,s4,s5,s6,s7,s8,s9,sA;

  int nw = w.size();
  int d = der.size();

  Dpar	= p[0];	// (1/ns)
  Dper	= p[1];	// (1/ns)
  cos2	= p[2]; //
  S2	= p[3];
  te	= p[4];	// (ns)
  Rex	= p[5];	// (1/s)

  tc[0] = 1./(6.*Dper);
  tc[1] = 1./(5.*Dper+1.*Dpar);
  tc[2] = 1./(2.*Dper+4.*Dpar);

  dtcdDper[0] = -tc[0]/Dper;
  dtcdDper[1] = -tc[1]*5./(5.*Dper+1.*Dpar);
  dtcdDper[2] = -tc[2]*2./(2.*Dper+4.*Dpar);

  dtcdDpar[0] = 0.;
  dtcdDpar[1] = -tc[1]*1./(5.*Dper+1.*Dpar);
  dtcdDpar[2] = -tc[2]*4./(2.*Dper+4.*Dpar);

  A[0] = 0.25*SQR(3.*cos2 - 1.);
  A[1] = 3.*(1.-cos2)*cos2;
  A[2] = 0.75*SQR(1.-cos2);

  dAdcos2[0] = 1.5*(3.*cos2 -1.);
  dAdcos2[1] = 3.*(1.-2.*cos2);
  dAdcos2[2] = -1.5*(1.-cos2);

  for (int i=0; i<nw; i++) {

    s0 = s1 = s4 = s5 = s6 = s7 = s8 = s9 = sA = 0.;

    for (int k=0;k<3;k++) {

      if (tc[k] > 0. && A[k] > 0.) {

	p0 = 1.0/(1.0+SQR(w[i]*tc[k])); // unit-less
	p1 = tc[k]*p0;// (ns)
	s0 += A[k]*p1; // (ns)
	if (d != 0) {
	  s5 += p1*dAdcos2[k]; // dtheta
	  s7 += A[k]*(p0-2.*SQR(w[i]*p1))*dtcdDper[k]; // dDper
	  s9 += A[k]*(p0-2.*SQR(w[i]*p1))*dtcdDpar[k]; // dDpar
	  }

	if (te > 0.) {
	  t  = te*tc[k]/(te+tc[k]);// (ns)
	  p2 = t/(1. + SQR(w[i]*t));// (ns)
	  s1 += A[k]*p2;// (ns)
	  if (d != 0) {
	    s4 += (p2/te - p2/(te+tc[k]) -2.0*SQR(w[i]*p2*t/te));
	    s6 += p2*dAdcos2[k]; // dcos2
	    s8 += A[k]*(p2/tc[k] - p2/(te+tc[k]) -2.0*SQR(w[i]*p2*t/tc[k]))
		  *dtcdDper[k]; // dDper
	    sA += A[k]*(p2/tc[k] - p2/(te+tc[k]) -2.0*SQR(w[i]*p2*t/tc[k]))
		  *dtcdDpar[k]; // dDpar
	    } 
	  } // te > 0

	} // tc > 0

      } // k = 0..2
  
    jw[i] = fac*(S2*s0 + (1.0-S2)*s1);// (s/rad)

    if (d != 0) {
      der[i*6+0] = fac*(S2*s9 + (1.0-S2)*sA); // dDpar (ns)
      der[i*6+1] = fac*(S2*s7 + (1.0-S2)*s8); // dDper (ns)
      der[i*6+2] = fac*(S2*s5 + (1.0-S2)*s6); // dcos2
      der[i*6+3] = fac*(s0 - s1);	      // S2
      der[i*6+4] = fac*(1.0-S2)*s4;	      // te (ns)
      der[i*6+5] = 0.;			      // Rex
      }

    } // i
}

//
// Local Axially Symmetric: Jw (w, tiso, Dr, cos2, S2, te, Rex)
//
void Jw_Local_XSYM_2 (const vecd &w, const vecd &p, const vecb &b,
	const void *data, vecd &jw, vecd &der)
{
  const double fac = 0.4e-9; // 2/5
  double tiso,Dr,cos2,t,S2,te,Rex;
  double tc[3],A[3],dtcdtiso[3],dtcdDr[3],dAdcos2[3];
  double p0,p1,p2,s0,s1,s4,s5,s6,s7,s8,s9,sA;

  int nw = w.size();
  int d = der.size();

		// Diso = (Dpar+2*Dper)/3
		// tiso = 1/(6*Diso) = 0.5/(Dpar+2*Dper)
  tiso	= p[0];	// 0.5/(Dpar+2.*Dper) (ns)
  Dr  	= p[1];	// Dpar/Dper
  cos2 	= p[2]; // 
  S2	= p[3];
  te	= p[4];	// (ns)
  Rex	= p[5];	// (1/s)

  tc[0] = tiso*(2.+Dr)/(3.0);
  tc[1] = tiso*(2.+Dr)/(2.5+0.5*Dr);
  tc[2] = tiso*(2.+Dr)/(1.0+2.0*Dr);

  dtcdtiso[0] = (2.+Dr)/(3.0);
  dtcdtiso[1] = (2.+Dr)/(2.5+0.5*Dr);
  dtcdtiso[2] = (2.+Dr)/(1.0+2.0*Dr);

  dtcdDr[0] = tiso/(3.0);
  dtcdDr[1] = (tiso-0.5*tc[1])/(2.5+0.5*Dr);
  dtcdDr[2] = (tiso-2.0*tc[2])/(1.0+2.0*Dr);

  A[0] = 0.25*SQR(3.*cos2 - 1.);
  A[1] = 3.*(1.-cos2)*cos2;
  A[2] = 0.75*SQR(1.-cos2);

  dAdcos2[0] = 1.5*(3.*cos2 -1.);
  dAdcos2[1] = 3.*(1.-2.*cos2);
  dAdcos2[2] = -1.5*(1.-cos2);

  for (int i=0; i<nw; i++) {

    s0 = s1 = s4 = s5 = s6 = s7 = s8 = s9 = sA = 0.;

    for (int k=0;k<3;k++) {

      if (tc[k] > 0. && A[k] > 0.) {

	p0 = 1.0/(1.0+SQR(w[i]*tc[k])); // unit-less
	p1 = tc[k]*p0;// (ns)
	s0 += A[k]*p1; // (ns)
	if (d != 0) {
	  s5 += p1*dAdcos2[k]; // dcos2
	  s7 += A[k]*(p0-2.*SQR(w[i]*p1))*dtcdDr[k]; // dDr
	  s9 += A[k]*(p0-2.*SQR(w[i]*p1))*dtcdtiso[k]; // dtiso
	  }

	if (te > 0.) {
	  t  = te*tc[k]/(te+tc[k]);// (ns)
	  p2 = t/(1. + SQR(w[i]*t));// (ns)
	  s1 += A[k]*p2;// (ns)
	  if (d != 0) {
	    s4 += (p2/te - p2/(te+tc[k]) -2.0*SQR(w[i]*p2*t/te));
	    s6 += p2*dAdcos2[k]; // dcos2
	    s8 += A[k]*(p2/tc[k] - p2/(te+tc[k]) -2.0*SQR(w[i]*p2*t/tc[k]))
		  *dtcdDr[k]; // dDr
	    sA += A[k]*(p2/tc[k] - p2/(te+tc[k]) -2.0*SQR(w[i]*p2*t/tc[k]))
		  *dtcdtiso[k]; // dtiso
	    } 
	  } // te > 0

	} // tc > 0

      } // k = 0..2
  
    jw[i] = fac*(S2*s0 + (1.0-S2)*s1);// (s/rad)

    if (d != 0) {
      der[i*6+0] = fac*(S2*s9 + (1.0-S2)*sA); // dtiso (ns)
      der[i*6+1] = fac*(S2*s7 + (1.0-S2)*s8); // dDr (ns)
      der[i*6+2] = fac*(S2*s5 + (1.0-S2)*s6); // dcos2
      der[i*6+3] = fac*(s0 - s1);	      // S2
      der[i*6+4] = fac*(1.0-S2)*s4;	      // te (ns)
      der[i*6+5] = 0.;			      // Rex
      }

    } // i
}

