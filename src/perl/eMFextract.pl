#!/usr/bin/perl

#   eMF Copyright 2009 Sung-Hun Bae
#
#   This file is part of eMF.
#   eMF is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   eMF is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.

#   You should have received a copy of the GNU General Public License
#   along with eMF.  If not, see <http://www.gnu.org/licenses/>

my @R = (); # results
my @P = (); # parameters to display
my $resid, $line_number, $idx, $line, $status, @c, @lines;

sub help 
{
  my $line;
  while ($line = <DATA>) { print STDERR "$line"; }
  exit ($_[0]);
}

if (!@ARGV) { help(0); }
open(I, $ARGV[0]);
@lines = <I>;
close (I);
if ( $#ARGV > 0 ) { @P = @ARGV[1..$#ARGV]; }

#
# reading eMF (version 1.0) output
#
foreach $line (@lines) 
{
  if ($line =~ m/^[^#]/ && $line !~ m/^(\s)*$/) 
  {
    chomp;
    $line =~ s/^\s+|\s+$//g;
    @c = split /\s+/, $line;
    if ($c[0] =~ m/^RESIDUE/) 
    {
      $resid = $c[1];
      $status = 0;
    }
    if ($line =~ m/DF=/ && $line =~ m/CHISQ=/) 
    {
      $status = 0; # reset
      if ($c[0] =~ m/^>$/) 
      {
	$idx=0;
	$status = 1; # selected
	$func = $c[1];
	$mbit = $c[2];
	# translate model bit for Lipari-Szabo
	if ($func =~ m/LSI/ || $func =~ m/LSX/ ) 
	{
	  $flag = substr($mbit,0,4);
	  if ($flag =~ m/1000/) {$mnum=1;}
	  if ($flag =~ m/1100/) {$mnum=2;}
	  if ($flag =~ m/1010/) {$mnum=3;}
	  if ($flag =~ m/1110/) {$mnum=4;}
	  if ($flag =~ m/1101/) {$mnum=5;}
	  $R{$resid}{mdl} = $mnum; # model number
	} 
	else
	{
	  $R{$resid}{mdl} = $mbit; # model bit flag
	}
      }
    }
    if ($status == 1 && $line !~ m/CHISQ=/ && $line !~ m/ZXY/) 
    {
      $R{$resid}{par}{$idx}=$c[0];
      $R{$resid}{val}{$idx}=$c[1];
      $R{$resid}{err}{$idx}=$c[2];
      $idx++;
    }
  } # if not comment
} # for each line

#
# print out 
#
if (! @P)  
{
  $line_number = 0;
  foreach $resid (sort {$a <=> $b} keys %R) {
    $line_number++;
    # header
    if ($line_number == 1) {
      printf "# %5s ","Resid";
      # sort numerically ascending
      foreach $idx (sort {$a <=> $b} keys %{$R{$resid}{par}}) {
	printf "%7s %7s ",$R{$resid}{par}{$idx}.".val",$R{$resid}{par}{$idx}.".err";
      }
      printf "Model\n";    
    }
    # data
    printf "%7d ",$resid;
    # sort numerically ascending
    foreach $idx (sort {$a <=> $b} keys %{$R{$resid}{par}}) {
      printf "%7.3f %7.3f ",$R{$resid}{val}{$idx},$R{$resid}{err}{$idx};
    }
    printf "%d\n",$R{$resid}{mdl};
  }
}
else
{
  $line_number = 0;
  foreach $resid (sort {$a <=> $b} keys %R) {
    $line_number++;
    # header
    if ($line_number == 1) {
      printf "# %5s ","Resid";
      foreach $param (@P) { 
	if ($param =~ m/model/i) {
	  printf "%7s ","Model";
	}
	else {
	  foreach $idx (sort {$a <=> $b} keys %{$R{$resid}{par}}) {
	    if ($R{$resid}{par}{$idx} =~ m/$param/i) {
	      printf "%7s %7s ",$R{$resid}{par}{$idx}.".val",$R{$resid}{par}{$idx}.".err";
	    }
	  }
	}
      }
      printf "\n";
    }
    # data
    printf "%7d ",$resid;
    foreach $param (@P) {
      if ($param =~ m/model/i) {
	printf "%7d ",$R{$resid}{mdl};
      }
      else {
	foreach $idx (sort {$a <=> $b} keys %{$R{$resid}{par}}) {
	  if ($R{$resid}{par}{$idx} =~ m/$param/i) {
	    printf "%7.3f %7.3f ",$R{$resid}{val}{$idx},$R{$resid}{err}{$idx};
	  }
	}
      }
    }
    printf "\n";
  }
}

exit(0);

__DATA__

                                                Sung-Hun Bae, Oct. 2009

        Extract selected parameters(model) from eMF output file

        usage:
        eMFextract.pl <eMF output file> [parameter name or 'model'] ...
        
        ex:
        eMFextract.pl a.out
        show all parameters

        eMFextract.pl a.out model
        'model' returns model numbers for extended Lipari-Szabo models
        or parameter bit flags otherwise
        
        eMFextract.pl a.out S2s S2f te Rex model
	show S2s, S2f, te, Rex, and model

