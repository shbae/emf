#!/usr/bin/perl 

#   eMF Copyright 2009 Sung-Hun Bae
#
#   This file is part of eMF.
#   eMF is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   eMF is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.

#   You should have received a copy of the GNU General Public License
#   along with eMF.  If not, see <http://www.gnu.org/licenses/>

my $nd = 0;	# number of data files
@R = ();	# relaxation data

sub help {
	my $line;
	while ($line = <DATA>) {
		print STDERR "$line";	
		}
	exit ($_[0]);
	}

if (!@ARGV) { help(0); }

for ($i=0; $i < @ARGV; $i++) {
	if ($ARGV[$i] eq "-h") {
		help(0);
		}
	elsif ($ARGV[$i] eq "-d") {
		$z[$nd] = $ARGV[++$i];
		$B[$nd] = $ARGV[++$i];
		$d[$nd] = $ARGV[++$i];
		$nd++;
		}
	}

if ($nd == 0) { 
	die "Error: no input files\n"; 
	}

# read relaxation data files
for ($i=0; $i < $nd; $i++) {
	my $line;
	open (INPUT, $d[$i]) or die "Error: cannot open $d[$i]\n";
	@LINES = <INPUT>;
	foreach $line (@LINES) {
		if ($line =~ /^[^#]/) {
			chomp;
			$line =~ s/^\s+//;
			$line =~ s/\s+$//;
			@c = split /\s+/, $line;
			$R{$c[0]}{$z[$i]}{$B[$i]};
			$R{$c[0]}{$z[$i]}{$B[$i]}{value} = $c[1];
			$R{$c[0]}{$z[$i]}{$B[$i]}{error} = $c[2];
			}
		}
	close INPUT;
        }

# put in eMF format
# METHOD
# FUNCTION & SET
# VECTOR
# RESID & DATA
foreach my $resid (sort {$a <=> $b} keys %R) {
	printf "RESID $resid\n";
	foreach my $type (sort {$a <=> $b} keys %{$R{$resid}}) {
	 	foreach my $field (sort {$a <=> $b} keys %{$R{$resid}{$type}}) {
			printf "   DATA", $type;
			printf " %3d", $type;
			printf " %9.2f", $field;
			printf " %9.3f", $R{$resid}{$type}{$field}{value};
			printf " %9.3f\n", $R{$resid}{$type}{$field}{error};
			}
		}
	}


	
__DATA__

						Sung-Hun Bae, Oct. 2009
	Prepare input files for eMF

	usage:
	eMFprep.pl -d [data type] [B0(MHz)] [file] ...

        ex:
	eMFprep.pl -d 0 500 r1.dat \
		   -d 1 500 r2.dat \
		   -d 2 500 noe.dat \
		   -d 3 500 eta.dat

