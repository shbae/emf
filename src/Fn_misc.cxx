/*
    eMF Copyright 2008-2009 Sung-Hun Bae

    This file is part of eMF.

    eMF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    eMF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with eMF.  If not, see <http://www.gnu.org/licenses/>
*/

#include "dataset.h"
#include "funclib.h"

using namespace std;

void Exp (const vecd &x, const veci &z, const vecd &p, const vecb &b,
        const void *data, vecd &y, vecd &dyda)
{
double R = p[0];
double u = p[1];
double v = p[2];
for (int i=0;i<x.size();i++) {
	y[i] = u*exp(-R*x[i])+v;
	if (dyda.size() > 0) {
		dyda[3*i+0] = -x[i]*u*exp(-R*x[i]); // R
		dyda[3*i+1] = exp(-R*x[i]); // u
		dyda[3*i+2] = 1.; // v
		}
	}
}

// Icross/Iauto = tanh (eta_xy * delay)
void Tanh  (const vecd &x, const veci &z, const vecd &p, const vecb &b,
        const void *data, vecd &y, vecd &dyda)
{
double eta_xy = p[0];
for (int i=0; i<x.size(); i++) {
	y[i] = tanh(eta_xy * x[i]);
	if (dyda.size() > 0) {
		dyda[1*i+0] =  x[i]*(1-y[i]*y[i]);
		}
	}
}
