/*
    eMF Copyright 2008-2009 Sung-Hun Bae

    This file is part of eMF.

    eMF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    eMF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with eMF.  If not, see <http://www.gnu.org/licenses/>
*/

#include "dataset.h"
#include "funclib.h"

using namespace std;

//////////////////////////////////////////////////////////////////
//
// Define new functions here
//
// function code (e.g. LSI) : up to 5 characters
// parameter title (e.g. S2s) : up to 8 characters
//
// (idx, title, lower, upper, reset, grid, tol)
//
//////////////////////////////////////////////////////////////////

void Load_Functions (dataset &D)
{
  int idx;

  idx = D.define_function ("LSI",&Lipari_Szabo_Iso);
  D.define_parameter  (idx,"S2s",  0.  ,1.,1.,20,1.e-3);
  D.define_parameter  (idx,"te" ,  0.  ,10,0.,20,1.e-3);
  D.define_parameter  (idx,"Rex",  0.  ,50,0.,20,1.e-3);
  D.define_parameter  (idx,"S2f",  0.  ,1.,1.,20,1.e-3);
  D.define_parameter  (idx,"tc" ,  0.01,50,5.,20,1.e-3);

  idx = D.define_function ("LSX",&Lipari_Szabo_XSYM);
  D.define_parameter  (idx,"S2s",  0.  ,1. ,1.,20,1.e-3);
  D.define_parameter  (idx,"te" ,  0.  ,10 ,0.,20,1.e-3);
  D.define_parameter  (idx,"Rex",  0.  ,50 ,0.,20,1.e-3);
  D.define_parameter  (idx,"S2f",  0.  ,1. ,1.,20,1.e-3);
  D.define_parameter  (idx,"tc" ,  0.01,50 ,5.,20,1.e-3);
  D.define_parameter  (idx,"Dr" ,  0.01,20 ,1.,20,1.e-3);
  D.define_parameter  (idx,"phi" , 0.  ,6.283184,0.,20,1.e-3);
  D.define_parameter  (idx,"theta",0.  ,3.141592,0.,20,1.e-3);

  idx = D.define_function ("LSICC",&Lipari_Szabo_Iso_CC);
  D.define_parameter  (idx,"S2s",  0.  ,1.,1.,20,1.e-3);
  D.define_parameter  (idx,"te" ,  0.  ,10,0.,20,1.e-3);
  D.define_parameter  (idx,"Rex",  0.  ,50,0.,20,1.e-3);
  D.define_parameter  (idx,"S2f",  0.  ,1.,1.,20,1.e-3);
  D.define_parameter  (idx,"S2cd",-1.  ,1. ,1.,40,1.e-3);
  D.define_parameter  (idx,"beta",    0.  ,3.141592 ,0.34906585,20,1.e-3);
  D.define_parameter  (idx,"tc" ,  0.01,50,5.,20,1.e-3);

  idx = D.define_function ("LSXCC",&Lipari_Szabo_XSYM_CC);
  D.define_parameter  (idx,"S2s",  0.  ,1. ,1.,20,1.e-3);
  D.define_parameter  (idx,"te" ,  0.  ,10 ,0.,20,1.e-3);
  D.define_parameter  (idx,"Rex",  0.  ,50 ,0.,20,1.e-3);
  D.define_parameter  (idx,"S2f",  0.  ,1. ,1.,20,1.e-3);
  D.define_parameter  (idx,"S2cd",-1.  ,1. ,1.,40,1.e-3);
  D.define_parameter  (idx,"beta",    0.  ,3.141592 ,0.34906585,20,1.e-3);
  D.define_parameter  (idx,"tc" ,  0.01,50 ,5.,20,1.e-3);
  D.define_parameter  (idx,"Dr" ,  0.01,20 ,1.,20,1.e-3);
  D.define_parameter  (idx,"phi" , 0.  ,6.283184,0.,20,1.e-3);
  D.define_parameter  (idx,"theta",0.  ,3.141592,0.,20,1.e-3);

  idx = D.define_function ("CC",&Cole_Cole);
  D.define_parameter  (idx,"tc" ,0.01,50,5,20,1.e-3);
  D.define_parameter  (idx,"e"  ,0.  ,1.,1,20,1.e-3);
  D.define_parameter  (idx,"S2" ,0.  ,1.,1,20,1.e-3);
  D.define_parameter  (idx,"te" ,0.  ,10,0,20,1.e-5);
  D.define_parameter  (idx,"Rex",0.  ,50,0,20,1.e-3);

  idx = D.define_function ("Loc2",&Local_2);
  D.define_parameter  (idx,"tc1",0.01,50,5,20,1.e-3);
  D.define_parameter  (idx,"tc2",0.01,50,5,20,1.e-3);
  D.define_parameter  (idx,"c"  ,0.  ,1.,1,20,1.e-3);
  D.define_parameter  (idx,"S2" ,0.  ,1.,1,20,1.e-3);
  D.define_parameter  (idx,"te" ,0.  ,10,0,20,1.e-5);
  D.define_parameter  (idx,"Rex",0.  ,50,0,20,1.e-3);

  idx = D.define_function ("LocX1",&Local_XSYM_1);
  D.define_parameter  (idx,"Dpar",0.001,2.,1,20,1.e-3);
  D.define_parameter  (idx,"Dper",0.001,2.,1,20,1.e-3);
  D.define_parameter  (idx,"cos2",0.   ,1.,0,20,1.e-3);
  D.define_parameter  (idx,"S2"  ,0.   ,1.,1,20,1.e-3);
  D.define_parameter  (idx,"te"  ,0.   ,10,0,20,1.e-5);
  D.define_parameter  (idx,"Rex" ,0.   ,50,0,20,1.e-3);

  idx = D.define_function ("LocX2",&Local_XSYM_2);
  D.define_parameter  (idx,"tiso",0.01,50,5,20,1.e-3);
  D.define_parameter  (idx,"Dr"  ,0.01,99,1,20,1.e-3);
  D.define_parameter  (idx,"cos2",0.  ,1.,0,20,1.e-3);
  D.define_parameter  (idx,"S2"  ,0.  ,1.,1,20,1.e-3);
  D.define_parameter  (idx,"te"  ,0.  ,10,0,20,1.e-5);
  D.define_parameter  (idx,"Rex" ,0.  ,50,0,20,1.e-3);

  idx = D.define_function ("Exp",&Exp);
  D.define_parameter  (idx,"R",-HUGE,HUGE,1,20,1.e-3);
  D.define_parameter  (idx,"a",-HUGE,HUGE,0,20,1.e-3);
  D.define_parameter  (idx,"b",-HUGE,HUGE,0,20,1.e-3);

  idx = D.define_function ("Tanh",&Tanh);
  D.define_parameter  (idx,"eta_xy",-HUGE,HUGE,1,20,1.e-3);
}
