/*
    eMF Copyright 2009 Sung-Hun Bae

    This file is part of eMF.

    eMF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    eMF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with eMF.  If not, see <http://www.gnu.org/licenses/>
*/

#include "dataset.h"
#include "funclib.h"
#include "methods.h"
#include "util.h"

#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

using namespace std;

double sa (void *data)
{
  dataset *D = (dataset *)data;
  int nv = D->work_nv;

  if (nv == 0) return -1;

  extern gsl_rng * rng;

  gsl_vector *v = gsl_vector_alloc (nv);
  gsl_vector *v_trial = gsl_vector_alloc (nv);

  double K,chisq, chisq_trial, delta_chisq, dp, rand;
  
  D->set_v (v);
  chisq = f_chisq (v, data);

  K = D->mo.sa_start;

  do {

    unsigned int iter = 0;

    do {
      iter++;
      for (int i=0;i<v_trial->size;i++) {
	dp = 2.*gsl_rng_uniform (rng)-1.;	  // [-1,1)
	gsl_vector_set (v_trial,i, gsl_vector_get (v,i)+0.1*dp);
	}
      chisq_trial = f_chisq (v_trial, data);
      delta_chisq = chisq_trial - chisq;
      // accept
      rand = gsl_ran_flat (rng, 0., 1.);
      if ((delta_chisq < 0.) || rand < exp(-delta_chisq/K)) {
	gsl_vector_memcpy (v, v_trial);
	chisq = chisq_trial;
	}
      } while (iter < D->mo.sa_iter);

    K /= D->mo.sa_damping;

    } while (K < D->mo.sa_stop);

  D->store_v (v, NULL, chisq);

  gsl_vector_free (v);
  gsl_vector_free (v_trial);

  return chisq;
}
