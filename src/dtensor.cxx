/*
    eMF Copyright 2008-2009 Sung-Hun Bae

    This file is part of eMF.

    eMF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    eMF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with eMF.  If not, see <http://www.gnu.org/licenses/>
*/

#include "dataset.h"
#include "funclib.h"
#include "methods.h"
#include "util.h"

using namespace std;

//
// Optimize Rotational Diffusion Tensor, Drr
// using local isotropic diffusion approximation
//
// variables: Dper(0), Dpar(1), theta(2), phi(3)
//

double quad_xsym  (const int,void *,vecd &,vecd &);
int quad_xsym_f   (const gsl_vector *,void *,gsl_vector *);
int quad_xsym_df  (const gsl_vector *,void *,gsl_matrix *);
int quad_xsym_fdf (const gsl_vector *,void *,gsl_vector *,gsl_matrix *);

void optimize_Drr (dataset &D) 
{
extern gsl_rng * rng;
int r,f,m,p,v,nx,c;
double chisq,tisoloc_val,tisoloc_err;
vector < vector < PARAMETER > > par;
vector <double> x2;

// calculate Di(=1/(6tloc))
clog << "\nFitting local isotropic tc ......\n";
for (r=0;r<D.resid.size();r++) {
	if (D.resid[r].flag) {
	    for (f=0;f<D.resid[r].f.size();f++) {
		// assign residue and function for fitting
		D.assign_work (r,f);
		// fitting procedure
		for (m=0;m<D.Qm.size();m++) {
			chisq = (D.Qm[m].ptr) (&D);
			} // methods
		// store fitted parameters
		for (p=0;p<D.resid[r].f[f].p.size();p++) {
			D.resid[r].f[f].p[p].value = D.work_p[p].value;
			D.resid[r].f[f].p[p].error = D.work_p[p].error;
			}
		// store fitting statistics
		D.resid[r].f[f].s.dof = D.work_nx - D.work_nv;
		D.resid[r].f[f].s.chisq = chisq;	// last chisq
		D.resid[r].f[f].s.bic = chisq + 
			D.work_nv*log((double)D.work_nx);
		D.resid[r].f[f].s.aic = chisq + D.work_nv*2;
		} // functions
      	     } // flag
	} // residues

// select best function using Bayesian Information Criteria
D.select (BIC); 

// Monte-Carlo simulations for error estimation
clog << "\nRunning Monte-Carlo simulations for error estimation ......\n";
for (r=0;r<D.resid.size();r++) {
	if (D.resid[r].flag) {
		f = D.resid[r].select;
		D.assign_work (r, f);
		for (m=0;m<D.Qm.size();m++) {
			x2.resize(0);
			par.resize(0);
			c = 0;
			while (c < D.Qm[m].MC) {
				// generate synthetic data set using 
				// experimental error
				D.synthetic (rng);
				// fit synthetic data set
				chisq = (D.Qm[m].ptr) (&D);
				// record fitting results
				if (!isnan(chisq)) {
					c++;
					x2.push_back (chisq);
					par.push_back (D.work_p);
					}
				} 
			if (c > 1) D.store_mc (c, x2, par);
			} // methods
		// store error
		for (p=0;p<D.resid[r].f[f].p.size();p++)
			D.resid[r].f[f].p[p].error = D.work_p[p].error;
		} // flag
	} // residues

// get Di
clog << "\nLocal Isotropic Tumbling Time (tloc=1/6Di)\n";
clog << setw(6) << right << "Resid#" << " ";
clog << resetiosflags (ios_base::floatfield);
clog << fixed << setprecision(4);
clog << setw(10) << right << "Di" << " ";
clog << setw(10) << right << "dDi" << " ";
clog << fixed << setprecision(4);
clog << setw(10) << right << "vector X" <<  " ";
clog << setw(10) << right << "vector Y" << " ";
clog << setw(10) << right << "vector Z" << " ";
clog << endl;

for (nx=0,r=0;r<D.resid.size();r++) {
	if (D.resid[r].flag) {
	    v = D.p_index (D.resid[r].f[D.resid[r].select].title, "tc");
	    tisoloc_val = D.resid[r].f[D.resid[r].select].p[v].value;
	    tisoloc_err = D.resid[r].f[D.resid[r].select].p[v].error;
	    D.resid[r].Di = 1./(6.*tisoloc_val);
	    D.resid[r].dDi = 1./(6.*tisoloc_val) * (tisoloc_err/tisoloc_val);
	
	    cout << setw(6) << right;
	    cout << D.resid[r].num << " ";
	    cout << resetiosflags (ios_base::floatfield);
	    cout << fixed << setprecision(4);
	    cout << setw(10) << right << D.resid[r].Di << " ";
	    cout << setw(10) << right << D.resid[r].dDi << " ";
	    cout << fixed << setprecision(4);
	    cout << setw(10) << right << D.resid[r].vec[0] << " ";
	    cout << setw(10) << right << D.resid[r].vec[1] << " ";
	    cout << setw(10) << right << D.resid[r].vec[2] << " ";
	    cout << endl;
	    nx++;
      	    } // flag
	} // r

vecd val,err;
val.resize (4);
err.resize (4);
val[0] = 0.02;
val[1] = 0.04;
val[2] = 0;
val[3] = 0;

chisq = quad_xsym (nx, &D, val, err);


// MC


clog << "\nAxially symmetric diffusion\n";
clog << resetiosflags (ios_base::floatfield);
clog << fixed << setprecision(4);
clog << setw(15) << left << "CHISQ/DOF";
clog << setw(15) << right << chisq/(nx-4) << endl;
clog << fixed << setprecision(4);
clog << setw(15) << left << "Dper";
clog << setw(15) << right << val[0];
clog << setw(15) << right << err[0] << endl;
clog << setw(15) << left << "Dpar";
clog << setw(15) << right << val[1];
clog << setw(15) << right << err[1] << endl;
clog << setw(15) << left << "Theta";
clog << setw(15) << right << val[2];
clog << setw(15) << right << err[2] << endl;
clog << setw(15) << left << "Phi";
clog << setw(15) << right << val[3];
clog << setw(15) << right << err[3] << endl;
clog << setw(15) << left << "Dpar/Dper";
clog << setw(15) << right << val[1]/val[0] << endl;
}

double quad_xsym (const int nx, void *data, vecd &val, vecd &err)
{
  const int nv = val.size();
  dataset *D = (dataset *)data;
  gsl_vector *v = gsl_vector_alloc (nv);
  gsl_matrix *covar = gsl_matrix_alloc (nv,nv);
  unsigned int iter = 0;
  int status;
  double chisq;

  for (int i=0;i<nv;i++) 
    gsl_vector_set (v,i,val[i]);

  gsl_multifit_function_fdf f;
  gsl_multifit_fdfsolver *s;
  const gsl_multifit_fdfsolver_type *T;

  f.f       = &quad_xsym_f;
  f.df      = &quad_xsym_df;
  f.fdf     = &quad_xsym_fdf;
  f.n       = nx;
  f.p       = nv;
  f.params  = data;

  T = gsl_multifit_fdfsolver_lmsder;
  s = gsl_multifit_fdfsolver_alloc (T,nx,nv);
  gsl_multifit_fdfsolver_set (s, &f, v);

  do {
    iter++;
    status = gsl_multifit_fdfsolver_iterate (s);
    if (status) break;
    status = gsl_multifit_test_delta (s->dx, s->x, 1.e-15, 1.e-9);
    } while (status == GSL_CONTINUE && iter < 1.e+6);

  gsl_multifit_covar (s->J, 0.0, covar);
  chisq = pow(gsl_blas_dnrm2(s->f),2.);

  #ifdef INCREASE_COVAR_ERROR
  // if the chisq shows a poor fit (i.e. chisq/dof >> 1) then 
  // the error estimates obtained from the covariance matrix will be too small.
  // as a common way of increasing the errors for a poor fit, 
  // the error estimates are multiplied by \sqrt{chisq/dof}.
  double fac = ((nx-nv) == 0 ? 1 : GSL_MAX_DBL (1,sqrt(chisq/(nx-nv))));
  #endif
  for(int i=0;i<nv;i++) {
	val[i] = gsl_vector_get (s->x, i);
        #ifdef INCREASE_COVAR_ERROR
	err[i] = fac*sqrt(gsl_matrix_get(covar,i,i));
        #else
	err[i] = sqrt(gsl_matrix_get(covar,i,i));
        #endif
	}

  gsl_multifit_fdfsolver_free (s);
  gsl_vector_free (v);
  gsl_matrix_free (covar);

  return chisq;
}

int quad_xsym_f (const gsl_vector *v, void *data, gsl_vector *chi)
{
return quad_xsym_fdf (v,data,chi,NULL);
}

int quad_xsym_df (const gsl_vector *v, void *data,gsl_matrix *jacob)
{
return quad_xsym_fdf (v,data,NULL,jacob);
}

int quad_xsym_fdf (const gsl_vector *v, void *data,
	gsl_vector *chi, gsl_matrix *jacob)
{
dataset *D = (dataset *)data;
double Dper = gsl_vector_get(v,0);
double Dpar = gsl_vector_get(v,1);
double sinth = sin(gsl_vector_get(v,2)); // sin(theta)
double costh = cos(gsl_vector_get(v,2)); // cos(theta)
double sinph = sin(gsl_vector_get(v,3)); // sin(phi)
double cosph = cos(gsl_vector_get(v,3)); // cos(phi)
double f1,f2;
int i,r;
for (i=0, r=0;r < D->resid.size();r++) {
	if (D->resid[r].flag) {
		f1 = sinth*cosph*D->resid[r].vec[0]+ 
		     sinth*sinph*D->resid[r].vec[1]+
                     costh*D->resid[r].vec[2];
		f2 = SQR(f1)/2.;
		if (chi != NULL) {
			gsl_vector_set (chi, i, 
				((Dper+Dpar)/2.+(Dper-Dpar)*f2-D->resid[r].Di)
				/D->resid[r].dDi);
			}
		if (jacob != NULL) {
			gsl_matrix_set (jacob, i, 0, 0.5+f2);	// Dper
			gsl_matrix_set (jacob, i, 1, 0.5-f2);	// Dpar
			gsl_matrix_set (jacob, i, 2, 
				( costh*cosph*D->resid[r].vec[0]
			 	 +costh*sinph*D->resid[r].vec[1]
			 	 -sinth*D->resid[r].vec[2])*(Dper-Dpar)*f1);
			gsl_matrix_set (jacob, i, 3, 
				(-sinth*sinph*D->resid[r].vec[0]
			 	 +sinth*cosph*D->resid[r].vec[1])*
				(Dper-Dpar)*f1 );
			}
		i++;
		} // flag
	} // resid
	return GSL_SUCCESS;
}
