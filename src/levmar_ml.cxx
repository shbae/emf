/*
    eMF Copyright 2008-2009 Sung-Hun Bae

    This file is part of eMF.

    eMF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    eMF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with eMF.  If not, see <http://www.gnu.org/licenses/>
*/

#include "dataset.h"
#include "methods.h"

#include "levmar_ml.h"

using namespace std;

double levmarML (void *data)
{
  dataset *D = (dataset *)data;
  int nv = D->work_nv;
  int nx = D->work_nx;

  if (nv == 0) return -1.;

  unsigned int iter;
  double chisq;

  double opt[LM_OPTS_SZ], info[LM_INFO_SZ];

  opt[0] = D->mo.levmar_ML_ini_mu;    // initial \mu
  opt[1] = D->mo.levmar_ML_stop_Je;   // stop threshold for ||J^T e||_inf
  opt[2] = D->mo.levmar_ML_stop_dp2;  // stop threshold for ||Dp||_2
  opt[3] = D->mo.levmar_ML_stop_e2;   // stop threshold for ||e||_2
  opt[4] = D->mo.levmar_ML_stop_jac;  // for finite difference jacobian
  
  double *v = new double [nv];
  double *y = new double [nx];
  double *L = new double [nv];
  double *U = new double [nv];

  gsl_vector_view vv = gsl_vector_view_array (v, nv); 
  gsl_vector_view vL = gsl_vector_view_array (L, nv); 
  gsl_vector_view vU = gsl_vector_view_array (U, nv); 

  double *covar = new double [nv*nv];

  gsl_matrix_view vcovar = gsl_matrix_view_array (covar, nv, nv);

  D->set_v (&vv.vector);
  D->set_bc (&vL.vector, &vU.vector);

  for (int i=0;i<nx;i++)
    y[i] = D->work_y[i];

  // Levenberg-Marquardt using derivatives
  iter = dlevmar_der (&lm_ML_f, &lm_ML_df, v, y, nv, nx,
    D->mo.levmar_ML_stop_iter, opt, info, NULL, covar, data);

  // calculate chisq
  lm_ML_f (v,y,nv,nx,data);   
  chisq = 0.0;
  for (int i=0;i<nx;i++)
    chisq += (y[i] - D->work_y[i])*(y[i]-D->work_y[i]);
  
  D->store_v (&vv.vector, &vcovar.matrix, chisq);

  delete [] v;
  delete [] L;
  delete [] U;
  delete [] y;
  delete [] covar;

  return chisq;
}

double levmarML_bc (void *data)
{
  dataset *D = (dataset *)data;
  int nv = D->work_nv;
  int nx = D->work_nx;

  if (nv == 0) return -1.;

  unsigned int iter;
  double chisq;

  double opt[LM_OPTS_SZ], info[LM_INFO_SZ];

  opt[0] = D->mo.levmar_ML_ini_mu;    // initial \mu
  opt[1] = D->mo.levmar_ML_stop_Je;   // stop threshold for ||J^T e||_inf
  opt[2] = D->mo.levmar_ML_stop_dp2;  // stop threshold for ||Dp||_2
  opt[3] = D->mo.levmar_ML_stop_e2;   // stop threshold for ||e||_2
  opt[4] = D->mo.levmar_ML_stop_jac;  // for finite difference jacobian
  
  double *v = new double [nv];
  double *y = new double [nx];
  double *L = new double [nv];
  double *U = new double [nv];

  gsl_vector_view vv = gsl_vector_view_array (v, nv); 
  gsl_vector_view vL = gsl_vector_view_array (L, nv); 
  gsl_vector_view vU = gsl_vector_view_array (U, nv); 

  double *covar = new double [nv*nv];

  gsl_matrix_view vcovar = gsl_matrix_view_array (covar, nv, nv);

  D->set_v (&vv.vector);
  D->set_bc (&vL.vector, &vU.vector);

  for (int i=0;i<nx;i++)
    y[i] = D->work_y[i];

  // Levenberg-Marquardt using derivatives and box constraints
  iter = dlevmar_bc_der (&lm_ML_f, &lm_ML_df, v, y, nv, nx, L, U, NULL,
    D->mo.levmar_ML_stop_iter, opt, info, NULL, covar, data);

  // calculate chisq
  lm_ML_f (v,y,nv,nx,data);   
  chisq = 0.0;
  for (int i=0;i<nx;i++)
    chisq += (y[i] - D->work_y[i])*(y[i]-D->work_y[i]);
  
  D->store_v (&vv.vector, &vcovar.matrix, chisq);

  delete [] v;
  delete [] L;
  delete [] U;
  delete [] y;
  delete [] covar;

  return chisq;
}
