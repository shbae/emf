/*
    eMF Copyright 2008-2009 Sung-Hun Bae

    This file is part of eMF.

    eMF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    eMF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with eMF.  If not, see <http://www.gnu.org/licenses/>
*/

#include "dataset.h"
#include "methods.h"

using namespace std;

double conjgr (void *data)
{
  dataset *D = (dataset *)data;
  int nx = D->work_nx;
  int nv = D->work_nv;

  if (nv == 0) return -1.; // do nothing

  gsl_vector *v = gsl_vector_alloc (nv);
  D->set_v (v);

  const gsl_multimin_fdfminimizer_type *T;
  gsl_multimin_fdfminimizer *s;
  gsl_multimin_function_fdf f;

  f.f	    = &mmin_f;
  f.df	    = &mmin_df;
  f.fdf	    = &mmin_fdf;
  f.n	    = nv;
  f.params  = data;

  switch (D->mo.conjgr_algorithm) {
    case 0: // Fletcher-Reeves
      T = gsl_multimin_fdfminimizer_conjugate_fr;
      break;
    case 1: // Polak-Ribiere 
      T = gsl_multimin_fdfminimizer_conjugate_pr;
      break;
    case 2: // Broyden-Fletcher-Goldfarb-Shanno (BFGS)
      T = gsl_multimin_fdfminimizer_vector_bfgs;
      break;
    case 3: // Broyden-Fletcher-Goldfarb-Shanno (BFGS2)
      T = gsl_multimin_fdfminimizer_vector_bfgs2;
      break;
      }

  s = gsl_multimin_fdfminimizer_alloc (T,nv);
  gsl_multimin_fdfminimizer_set (s, &f, v, D->mo.conjgr_step_size, 
      D->mo.conjgr_ftol);

  unsigned int iter = 0;
  int status;

  do {
    iter++;
    status = gsl_multimin_fdfminimizer_iterate (s);
    if (status) break;
    status = gsl_multimin_test_gradient (s->gradient, D->mo.conjgr_stop_grad);
    } while (status == GSL_CONTINUE && iter < D->mo.conjgr_stop_iter);

  D->store_v (s->x,NULL,s->f);

  gsl_multimin_fdfminimizer_free (s);
  gsl_vector_free (v);

  return s->f;
}
