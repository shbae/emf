/*
    eMF Copyright 2008-2009 Sung-Hun Bae

    This file is part of eMF.

    eMF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    eMF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with eMF.  If not, see <http://www.gnu.org/licenses/>
*/

#ifndef	_FUNCLIB_H_
#define	_FUNCLIB_H_

#define _UF_ (const vecd &,const veci &,const vecd &,const vecb &,const void *,vecd &,vecd &)
#define _LF_ (const vecd &,const vecd &,const vecb &,const void *,vecd &,vecd &)

#define P2(x) ((3*(x)*(x)-1)/2)

using namespace std;

void Load_Functions (dataset &);

// Upper level Function
void Lipari_Szabo_Iso 		_UF_;
void Lipari_Szabo_XSYM 		_UF_;
void Lipari_Szabo_Iso_CC	_UF_;
void Lipari_Szabo_XSYM_CC	_UF_;
void Cole_Cole	      		_UF_;
void Local_2    		_UF_;
void Local_XSYM_1     		_UF_;
void Local_XSYM_2     		_UF_;

void Exp			_UF_;
void Tanh			_UF_;

// mediator
void relaxation (void (*Jw) _LF_,
	const vecd &,const veci &,const vecd &,const vecb &,
	const void *,vecd &,vecd &);

// Lower level Function
void Jw_Lipari_Szabo_Iso	_LF_;
void Jw_Lipari_Szabo_XSYM	_LF_;
void Jw_Lipari_Szabo_Iso_CC	_LF_;
void Jw_Lipari_Szabo_XSYM_CC	_LF_;
void Jw_Cole_Cole		_LF_;
void Jw_Lorentzian		_LF_;
void Jw_Local_2			_LF_;
void Jw_Local_XSYM_1		_LF_; 
void Jw_Local_XSYM_2 		_LF_;

#endif
