/*
    eMF Copyright 2008-2009 Sung-Hun Bae

    This file is part of eMF.

    eMF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    eMF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with eMF.  If not, see <http://www.gnu.org/licenses/>
*/

#ifndef _UTIL_H_
#define _UTIL_H_

using namespace std;

int parse (const char *, const char *, vector <string> &);

#define SQR(a) ((a)*(a))
#define MAX(a,b) ((a)>(b) ? (a):(b))
#define MIN(a,b) ((a)>(b) ? (b):(a))
#define DEG2RAD(a) ((a)*M_PI/180.)
#define RAD2DEG(a) ((a)*180./M_PI)

#endif
