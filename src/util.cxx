/*
    eMF Copyright 2009 Sung-Hun Bae

    This file is part of eMF.

    eMF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    eMF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with eMF.  If not, see <http://www.gnu.org/licenses/>
*/

#include <cstring>
#include <cstdlib>
#include <vector>
#include <string>

using namespace std;

int parse (const char *line, const char *delimit, vector <string> &c)
{
  char work[strlen(line)],*token;
  bool comment = false;
  int i;

  strcpy (work, line);
  c.resize(0);
  
  token = strtok (work, delimit);
  if (token == NULL) return 0;

  for (i=0;i<strlen(token);i++)
    if (token[i] == '#') {
      comment = true;
      break;
      }

  if (!comment) 
    c.push_back (token);
  else 
    return 0;

  while (token != NULL) {
    token = strtok (NULL, delimit);
    if (token != NULL) {
      for (i=0;i<strlen(token);i++)
	if (token[i] == '#') {
	  comment = true;
	  break;
	  }
      if (comment) 
	break;
      else
	c.push_back (token);
      }
    }
  return c.size();
}
