/*
    eMF Copyright 2008-2009 Sung-Hun Bae

    This file is part of eMF.

    eMF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    eMF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with eMF.  If not, see <http://www.gnu.org/licenses/>
*/

/*
  Local Anisotropic Model
*/

#include "dataset.h"
#include "funclib.h"
#include "util.h"

using namespace std;

void Local_2 (const vecd &x,const veci &z, const vecd &p, const vecb &b,
	const void *data, vecd &y, vecd &dyda)
{
dataset *D = (dataset *)data;
double x_min,x_max;

relaxation (&Jw_Local_2, x, z, p, b, data, y, dyda);

// Quadratic Scaling of Rex
D->get_x_range (x_min,x_max);
int np = p.size();
for (int i=0;i<x.size();i++) {
	if (z[i] == 1) {
		y[i] = y[i] + p[5]*SQR(x[i]/x_min); // R2 = R20 + Rex
		if (dyda.size() > 0)
			dyda[np*i+5] = SQR(x[i]/x_min);
		}
	}
}

void Jw_Local_2 (const vecd &w, const vecd &p, const vecb &b,
	const void *data, vecd &jw, vecd &der)
{
  const double fac = 0.4e-9; // 2/5
  double tc[2],A[2],te,S2,Rex;
  double dtcdtc1[2], dtcdtc2[2], dAdc[2];
  double t,p0,p1,p2,s0,s1,s4,s5,s6,s7,s8,s9,sA;

  int nw = w.size();
  int d = der.size();

  tc[0] = p[0];	      // tc1, (ns)
  tc[1] = p[1];	      // tc2, (ns)
  A[0]  = p[2];	      // c
  A[1]  = (1.-p[2]);  // 1-c 
  S2  = p[3];
  te  = p[4];	      // (ns)
  Rex = p[5];	      // (1/s)

  dtcdtc1[0] =  1.;
  dtcdtc1[1] =  0.;
  dtcdtc2[0] =  0.;
  dtcdtc2[1] =  1.;
  dAdc[0] =  1.;
  dAdc[1] = -1.;

  for (int i=0; i<nw; i++) {

    s0 = s1 = s4 = s5 = s6 = s7 = s8 = s9 = sA = 0.;

    for (int k=0;k<2;k++) {

      if (tc[k] > 0. && A[k] > 0.) {

	p0 = 1./(1.+SQR(w[i]*tc[k])); // unit-less
	p1 = tc[k]*p0;// (ns)
	s0 += A[k]*p1; // (ns)
	if (d != 0) {
	  s5 += p1*dAdc[k];
	  s7 += A[k]*(p0-2.*SQR(w[i]*p1))*dtcdtc2[k];
	  s9 += A[k]*(p0-2.*SQR(w[i]*p1))*dtcdtc1[k];
	  }

	if (te > 0.) {
	  t  = te*tc[k]/(te+tc[k]);// (ns)
	  p2 = t/(1.+SQR(w[i]*t));// (ns)
	  s1 += A[k]*p2;// (ns)
	  if (d != 0) {
	    s4 += (p2/te - p2/(te+tc[k]) -2.*SQR(w[i]*p2*t/te));
	    s6 += p2*dAdc[k];
	    s8 += A[k]*(p2/tc[k]-p2/(te+tc[k])-2.*SQR(w[i]*p2*t/tc[k]))
		  *dtcdtc2[k];
	    sA += A[k]*(p2/tc[k]-p2/(te+tc[k])-2.*SQR(w[i]*p2*t/tc[k]))
		  *dtcdtc1[k];
	    } 
	  } // te > 0

	} // tc > 0
      } // k = 0..1
  
    jw[i] = fac*(S2*s0 + (1.0-S2)*s1);// (s/rad)

    if (d != 0) {
      der[i*6+0] = fac*(S2*s9 + (1.0-S2)*sA);  // tc1 (ns)
      der[i*6+1] = fac*(S2*s7 + (1.0-S2)*s8);  // tc2 (ns)
      der[i*6+2] = fac*(S2*s5 + (1.0-S2)*s6);  // c
      der[i*6+3] = fac*(s0 - s1); // S2
      der[i*6+4] = fac*(1.0-S2)*s4;	// te (ns)
      der[i*6+5] = 0.; // Rex
      }

    } // i
}
