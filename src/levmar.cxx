/*
    eMF Copyright 2008-2009 Sung-Hun Bae

    This file is part of eMF.

    eMF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    eMF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with eMF.  If not, see <http://www.gnu.org/licenses/>
*/

#include "dataset.h"
#include "funclib.h"
#include "methods.h"
#include "util.h"
#include <gsl/gsl_multifit_nlin.h>
#include <gsl/gsl_blas.h>

using namespace std;

static void print_gsl_vector (gsl_vector *v)
{
  for (int i=0; i<v->size; i++) 
      printf("%12.4e ",gsl_vector_get(v,i));
    printf("\n");
}

static void print_gsl_matrix (gsl_matrix *m)
{
  for (int i=0; i<m->size1; i++) {
    for (int j=0; j<m->size2; j++)
      printf("%12.4e ",gsl_matrix_get(m,i,j));
    printf("\n");
  } 
}

double levmar (void *data)
{
  dataset *D = (dataset *)data;
  int nx = D->work_nx;
  int nv = D->work_nv;

  if (nv == 0) return -1.;

  gsl_vector *v = gsl_vector_alloc (nv);
  gsl_matrix *covar = gsl_matrix_alloc (nv,nv);

  double chisq;
  unsigned int iter = 0;
  int status;

  D->set_v (v);

  gsl_multifit_function_fdf f;
  gsl_multifit_fdfsolver *s;
  const gsl_multifit_fdfsolver_type *T;

  f.f	    = &lm_f;
  f.df	    = &lm_df;
  f.fdf	    = &lm_fdf;
  f.n	    = nx;
  f.p	    = nv;
  f.params  = data;

  T = gsl_multifit_fdfsolver_lmsder;
  s = gsl_multifit_fdfsolver_alloc (T,nx,nv);
  gsl_multifit_fdfsolver_set (s, &f, v);

  do {
    iter++;
    status = gsl_multifit_fdfsolver_iterate (s);
    if (status) break;
    status = gsl_multifit_test_delta (s->dx, s->x, 
	      D->mo.levmar_stop_grad,
	      D->mo.levmar_stop_delta);
    } while (status == GSL_CONTINUE && iter < D->mo.levmar_stop_iter);

  //debug
  //printf("Jacobian(nx*nv):%dx%d\n",nx,nv); print_gsl_matrix(s->J);

  gsl_multifit_covar (s->J, 0.0, covar);

  //debug
  //printf("Covar(nv*nv):\n"); print_gsl_matrix(covar);
  //printf("x(nx):\n"); print_gsl_vector(s->x);

  chisq = pow(gsl_blas_dnrm2(s->f),2.);

  D->store_v (s->x, covar, chisq);

  gsl_multifit_fdfsolver_free (s);
  gsl_vector_free (v);
  gsl_matrix_free (covar);

  return chisq;
}
