/*
    eMF Copyright 2008-2009 Sung-Hun Bae

    This file is part of eMF.

    eMF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    eMF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with eMF.  If not, see <http://www.gnu.org/licenses/>
*/

//////////////////////////////////////////////////////////////////
//  extended Lipari-Szabo with cross-correlated relaxation
//
//  Lipari and Szabo (1982) JACS 104, 4546-4559
//  Clore et al. (1990) JACS 112, 4989-4991
//
//////////////////////////////////////////////////////////////////

#include "dataset.h"
#include "funclib.h"
#include "util.h"

using namespace std;

//////////////////////////////////////////////////////////////////
//
//      Isotropic Diffusion / Cross-Correlation 
//
//////////////////////////////////////////////////////////////////

void Lipari_Szabo_Iso_CC (
	const vecd &x, const veci &z, const vecd &p, const vecb &b,
	const void *data, vecd &y, vecd &dyda )
{
dataset *D = (dataset *)data;
double x_min,x_max;

relaxation (&Jw_Lipari_Szabo_Iso_CC,x,z,p,b,data,y,dyda);

// Quadratic Scaling of Rex
D->get_x_range (x_min,x_max);
int np = p.size();
for (int i=0;i<x.size();i++) {
	if (z[i] == 1) {				// for R2
		y[i] = y[i] + p[2]*SQR(x[i]/x_min); 	// R2 = R2 + Rex
		if (dyda.size() > 0)
			dyda[np*i+2] = SQR(x[i]/x_min);
		}
	}
}

void Jw_Lipari_Szabo_Iso_CC (const vecd &W, const vecd &p, const vecb &b,
	const void *data, vecd &jw, vecd &der)
{
const double fac = 0.4e-9; // 2/5
double w,tc,te,t,S2s,S2f,Rex,S2cd,P2cosb;
double p0,p1,p2,s0,s1,s2,s3,s4;
int i;

S2s 	= p[0];
te  	= p[1]; // ns
Rex 	= p[2];
S2f 	= p[3];
S2cd	= p[4];
P2cosb 	= P2(cos(p[5])); // beta: rad
tc  	= p[6]; // ns

for (i=0;i<W.size();i++) {
	w = W[i];
	s0 = s1 = s2 = s3 = s4 = 0;
	if (tc > 0.) {
		p0 = 1.0/(1.0+SQR(w*tc)); // unit-less
		p1 = tc*p0;// (ns)
		s0 += p1;
		s2 += (p0-2.0*SQR(w*p1));
		if (te > 0.) {
			t  = te*tc/(te+tc);// (ns)
			p2 = t/(1.0+SQR(w*t));// (ns)
			s1 += p2;
			s3 += (p2/tc - p2/(te+tc) -2.*SQR(w*p2*t/tc));
			s4 += (p2/te - p2/(te+tc) -2.*SQR(w*p2*t/te));
			} // te > 0
		} // tc > 0

	if (i<5) jw[i] = fac*S2f*(S2s*s0 + (1-S2s)*s1);// (s/rad)
	else	 jw[i] = fac*S2f*(S2cd*s0 + (P2cosb-S2cd)*s1);// (s/rad)

	// auto-relaxation
	if (i<5 && der.size() > 0) {
		der[7*i+0] = fac*S2f*(s0 - s1); 			// S2s
		der[7*i+1] = fac*S2f*(1.-S2s)*s4;			// te
		der[7*i+2] = 0.; 					// Rex
		der[7*i+3] = fac*(S2s*s0 + (1.-S2s)*s1); 		// S2f
		der[7*i+4] = 0.; 					// S2cd
		der[7*i+5] = 0.;					// beta
		der[7*i+6] = fac*S2f*(S2s*s2 + (1.-S2s)*s3);		// tc
		} // derivatives
	// cross-correlated relaxation
	if (i>=5 && der.size() > 0) {
		der[7*i+0] = 0.; 					// S2s
		der[7*i+1] = fac*S2f*(P2cosb-S2cd)*s4;			// te
		der[7*i+2] = 0.; 					// Rex
		der[7*i+3] = fac*(S2cd*s0 + (P2cosb-S2cd)*s1); 		// S2f
		der[7*i+4] = fac*S2f*(s0 - s1); 			// S2cd
		der[7*i+5] = -fac*3.*cos(p[5])*sin(p[5]);		// beta
		der[7*i+6] = fac*S2f*(S2cd*s2 + (P2cosb-S2cd)*s3);	// tc
		} // derivatives
	} // i
}

//////////////////////////////////////////////////////////////////
//
//      Axially Symmetric Diffusion / Cross-Correlation
//
// 	tc = 1/(6Diso)
// 	Diso = (Dxx+Dyy+Dzz)/3
// 	Dxx = Dyy, Dzz, Dratio = Dzz/Dxx
// 	tc	= 1/(6Diso) 
//		= 0.5/(2Dxx+Dzz) 
//		= 0.5/(2Dxx+Dxx*Dratio)
//		= 0.5/Dxx/(2+Dratio)
// 	Dxx	= 0.5/tc/(2+Dratio)
//
//////////////////////////////////////////////////////////////////

void Lipari_Szabo_XSYM_CC (
	const vecd &x, const veci &z, const vecd &p, const vecb &b,
	const void *data, vecd &y, vecd &dyda )
{
dataset *D = (dataset *)data;
double x_min,x_max;


relaxation (&Jw_Lipari_Szabo_XSYM_CC,x,z,p,b,data,y,dyda);

// Quadratic Scaling of Rex
D->get_x_range (x_min,x_max);
int np = p.size();
for (int i=0;i<x.size();i++) {
	if (z[i] == 1) {
		y[i] = y[i] + p[2]*SQR(x[i]/x_min); // R2 = R20 + Rex
		if (dyda.size() > 0)
			dyda[np*i+2] = SQR(x[i]/x_min);
		}
	}
}

void Jw_Lipari_Szabo_XSYM_CC (const vecd &W, const vecd &p, const vecb &b,
	const void *data, vecd &jw, vecd &der)
{
dataset *D = (dataset *) data;
const double fac = 0.4e-9; // 2/5
double w,te,t,S2s,S2f,Rex;
double tc,Dratio,phi,theta,Dxx,Dzz,x,y,z;
double A[3],T[3],vx,vy,vz,a,c,s,c2;
double p0,p1,p2,s0,s1,s2,s3,s4,s9,sa,sb,sc,sd,se;
double dadp,dadt,dAda[3],dTdt[3],dTdD[3];
double S2cd, P2cosb;
int i,k;

S2s 	= p[0];
te  	= p[1];  // ns
Rex 	= p[2];
S2f 	= p[3];
S2cd	= p[4]; 
P2cosb 	= P2(cos(p[5])); // beta: rad
tc  	= p[6];  // ns 
Dratio 	= p[7];
phi 	= p[8]; // rad
theta 	= p[9]; // rad

if (fabs(tc) < 1.e-20) {
	cerr << "error: tc is zero\n";
	exit(1);
	}

x	= D->resid[D->work_resid].vec[0];
y	= D->resid[D->work_resid].vec[1];
z	= D->resid[D->work_resid].vec[2];
c   	= x*sin(theta)*sin(phi)-y*sin(theta)*cos(phi)+z*cos(theta); // cos(a)
a 	= acos(c); 	// rad
D->resid[D->work_resid].alpha = RAD2DEG(a);

Dxx	= 0.5/tc/(2.+Dratio);
Dzz	= Dxx*Dratio;
c2 	= c*c;		// cos(a)*cos(a)				

T[0] 	= 1./(6.*Dxx);
T[1] 	= 1./(5.*Dxx+1.*Dzz);
T[2] 	= 1./(2.*Dxx+4.*Dzz);
A[0] 	= SQR(1.5*c2-0.5);
A[1] 	= 3.*c2*(1.-c2);
A[2] 	= 0.75*SQR(1.-c2);

if (b[4]) {
	// dTdtc
	dTdt[0] = (2.+Dratio)/(3.0);
	dTdt[1] = (2.+Dratio)/(2.5+0.5*Dratio);
	dTdt[2] = (2.+Dratio)/(1.0+2.0*Dratio);
	}
if (b[5]) {
	// dTdDr
	dTdD[0] = tc/(3.);
	dTdD[1] = 1.5*tc/SQR(2.5+0.5*Dratio);
	dTdD[2] = -(3.0*tc)/SQR(1.0+2.0*Dratio);
	}
if (b[6] || b[7]) {
	// dAd(phi) 	= dAda * dad(phi)
	// dAd(theta) 	= dAda * dad(theta)
	s	= sin(a);	// sin(a);
	dAda[0] = -6*(1.5*c2-0.5)*c*s;
	dAda[1] = +6*(2*c2-1)*c*s;
	dAda[2] = +3*(1.-c2)*c*s;
	// dad(phi)
	if (b[6]) {
		dadp	= (fabs(a) < 1e-20 ? 0.0 :
        		sin(theta)*(x*cos(phi)+y*sin(phi))/(-s));
		}
	// dad(theta)
	if (b[7]) {
		dadt	= (fabs(a) < 1e-20 ? theta :
        		(cos(theta)*(x*sin(phi)-y*cos(phi))-sin(theta)*z)/(-s));
		}
	}

for (i=0;i<W.size();i++) {
	w = W[i];
	s0 = s1 = s2 = s3 = s4 = 0.;
	s9 = sa = sb = sc = sd = se = 0.;
	for (k=0; k<3; k++) {
		p0 = 1.0/(1.0+SQR(w*T[k])); // unit-less
		p1 = T[k]*p0; // (ns)
		s0 += A[k]*p1; // (ns)
		s2 += A[k]*(p0-2.*SQR(w*p1));
		s9 += A[k]*(p0-2.*SQR(w*p1))*dTdt[k];
		sb += A[k]*(p0-2.*SQR(w*p1))*dTdD[k];
		sd += p1*dAda[k];
		if (te > 0.) {
			t = te*T[k]/(te+T[k]);// (ns)
			p2 = t/(1.+SQR(w*t));// (ns)
			s1 += A[k]*p2; // (ns)
			s3 += A[k]*(p2/T[k]-p2/(te+T[k])-2.*SQR(w*p2*t/T[k]));
			s4 += A[k]*(p2/te-p2/(te+T[k])-2.*SQR(w*p2*t/te));
			sa += A[k]*(p2/T[k]-p2/(te+T[k])-2.*SQR(w*p2*t/T[k])) * 				dTdt[k];
			sc += A[k]*(p2/T[k]-p2/(te+T[k])-2.*SQR(w*p2*t/T[k])) * 				dTdD[k];
			se += p2*dAda[k];
			} // te > 0
		} // k
	if (i<5) jw[i] = fac*S2f*(S2s*s0 + (1.-S2s)*s1);// (s/rad)
	else	 jw[i] = fac*S2f*(S2cd*s0 + (P2cosb-S2cd)*s1);// (s/rad)

	if (i<5 && der.size() > 0) {
		der[10*i+0] = fac*S2f*(s0 - s1); 			// S2s
		der[10*i+1] = fac*S2f*(1.-S2s)*s4;			// te
		der[10*i+2] = 0.; 					// Rex
		der[10*i+3] = fac*(S2s*s0 + (1.-S2s)*s1); 		// S2f
		der[10*i+4] = 0.; 					// S2cd
		der[10*i+5] = 0.;					// beta
		der[10*i+6] = fac*S2f*(S2s*s9 + (1.-S2s)*sa); 		// tc
		der[10*i+7] = fac*S2f*(S2s*sb + (1.-S2s)*sc); 		// Dr
		der[10*i+8] = dadp*fac*S2f*(S2s*sd + (1.-S2s)*se);	// phi
		der[10*i+9] = dadt*fac*S2f*(S2s*sd + (1.-S2s)*se);	// theta
		} // derivatives
	if (i>=5 && der.size() > 0) {
		der[10*i+0] = 0.; 					// S2s
		der[10*i+1] = fac*S2f*(P2cosb-S2cd)*s4;			// te
		der[10*i+2] = 0.; 					// Rex
		der[10*i+3] = fac*(S2cd*s0 + (P2cosb-S2cd)*s1); 	// S2f
		der[10*i+4] = fac*S2f*(s0 - s1); 			// S2cd
		der[10*i+5] = -fac*3.*cos(p[5])*sin(p[5]);		// beta
		der[10*i+6] = fac*S2f*(S2cd*s9 + (P2cosb-S2cd)*sa); 	// tc
		der[10*i+7] = fac*S2f*(S2cd*sb + (P2cosb-S2cd)*sc); 	// Dr
		der[10*i+8] = dadp*fac*S2f*(S2cd*sd + (P2cosb-S2cd)*se);// phi
		der[10*i+9] = dadt*fac*S2f*(S2cd*sd + (P2cosb-S2cd)*se);// theta
		} // derivatives
	} // i
}
