/*
    eMF Copyright 2009 Sung-Hun Bae

    This file is part of eMF.

    eMF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    eMF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with eMF.  If not, see <http://www.gnu.org/licenses/>
*/

#include "dataset.h"
#include "funclib.h"
#include "methods.h"
#include "util.h"

using namespace std;

//
// quasi-random search
//

double qrand (void *data)
{
  dataset *D = (dataset *)data;
  int nv = D->work_nv;
  if (nv == 0) return -1.;

  gsl_qrng * qrng = gsl_qrng_alloc (gsl_qrng_sobol, nv);
  gsl_vector *v	= gsl_vector_alloc (nv);
  gsl_vector *v_best = gsl_vector_alloc (nv);
  double x2, chisq = 1.e+25;

  for (int i=0;i<D->mo.qrandom_trial;i++) {
	D->set_v_qrng (v, qrng);
	x2 = f_chisq (v, data);
	if (!isnan(x2) && x2 < chisq) {
		chisq = x2;
		gsl_vector_memcpy (v_best, v);
        	}
	}

  D->store_v (v_best, NULL, chisq);

  gsl_qrng_free (qrng);
  gsl_vector_free (v);
  gsl_vector_free (v_best);
  return chisq;
}

//
// random search
//

double rand (void *data)
{
  dataset *D = (dataset *)data;
  int nv = D->work_nv;
  if (nv == 0) return -1.;

  extern gsl_rng * rng;
  gsl_vector *v	= gsl_vector_alloc (nv);
  gsl_vector *v_best = gsl_vector_alloc (nv);
  double x2, chisq= 1.e+25;

  for (int i=0;i<D->mo.random_trial;i++) {
	D->set_v_rng (v, rng);
	x2 = f_chisq (v,data);
	if (!isnan(x2) && x2 < chisq) {
		chisq= x2;
		gsl_vector_memcpy (v_best, v);
		}
	}

  D->store_v (v_best, NULL, chisq);

  gsl_vector_free (v);
  gsl_vector_free (v_best);

  return chisq;
}
