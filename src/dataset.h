/*
    eMF Copyright 2008-2009 Sung-Hun Bae

    This file is part of eMF.

    eMF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    eMF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with eMF.  If not, see <http://www.gnu.org/licenses/>
*/

#ifndef	_DATASET_H_
#define	_DATASET_H_

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <cstdlib>
#include <cstring>
#include <vector>
#include <set>
#include <map>
#include <algorithm>
#include <gsl/gsl_math.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_multifit_nlin.h>
#include <gsl/gsl_multimin.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_qrng.h>
#include <gsl/gsl_randist.h>

using namespace std;

typedef vector <double>   vecd;
typedef vector <int>	  veci;
typedef vector <bool>	  vecb;
typedef enum {
	BIC, AIC, AICc
	} criterion;
typedef struct {
	int     random_trial;
	int     qrandom_trial;
	int 	rsimplex_trial;
	int     simplex_stop_iter;
	double  simplex_stop_size;
	int     levmar_stop_iter;
	double  levmar_stop_grad;
	double  levmar_stop_delta;
	int     levmar_ML_stop_iter;
	double  levmar_ML_ini_mu;
	double  levmar_ML_stop_Je;
	double  levmar_ML_stop_dp2;
	double  levmar_ML_stop_e2;
	double  levmar_ML_stop_jac;
	int     conjgr_algorithm;
	int     conjgr_stop_iter;
	double  conjgr_step_size;
	double  conjgr_ftol;
	double  conjgr_stop_grad;
	int     bc_OOL_stop_iter;
	double  sa_start;
	double  sa_damping;
	double  sa_stop;
	int     sa_iter;
	} opts;

typedef void (*fptr) (const vecd &x,const veci &z,
	const vecd &p, const vecb &b,
	const void *data, vecd &f, vecd &der);	// function pointer
typedef double (*mptr) (void *data);		// method pointer
typedef struct {
	string title;		// title of parameter
	bool flag;		// to be fitted(1) or not(0)
	double value,error,lower,upper,tol,mean;
	int  ngrid;		// for grid search
	} PARAMETER;
typedef struct {
	int  dof;
	double chisq,bic,aic,aicc;
	} STAT;			// statistics
typedef struct {
	string title;		// title of function
	fptr ptr;		// pointer
	vector <PARAMETER> p;	// set of parameters
	STAT s;			// statistics for goodness of fit 
	string bitstr;		// bit string for parameter flag
	} FUNCTION;
typedef struct {
	int  num;		// residue number
	bool flag;		// to be used(1) or not(0)
	vector <vecd> c;	// data
	vector <double> x,y,dy;	// data
	vector <int> z;		// data
	vector <FUNCTION> f;	// models
	double alpha, vec[3];	// bond vector
	double Di,dDi;		// local isotropic diffusion Di = 1/(6tloc)
	int select;		// best model
	} RESIDUE;
typedef struct {
	string title;		// title of method
	mptr ptr;		// pointer
	int  MC;		// Monte Carlo simulations
	} METHOD;
typedef struct {
	int resSeq;		// atom residue number
	vector <double> coor;	// atom coordinates
	vector <string> name;	// atom name
	} ATOM;

class dataset {
	private:
	protected:
	public:

	bool	optimize_Drr;

	int 	define_function (const string, const fptr);
	int 	f_index  (const string);
	int 	f_index  (const fptr);

	int 	define_method (const string, const mptr);
	int 	m_index(const string);

	void 	define_parameter (const int func, const string name, 
		const double lower, const double upper, const double reset,
		const int ngrid, const double tolerance);
	int 	p_index (const string, const string);

	vector 	<FUNCTION> 	Rf;	// Repository of functions
	vector 	<FUNCTION> 	Qf;	// Queue of functions
	vector 	<METHOD> 	Rm; 	// Repository of methods
	vector 	<METHOD> 	Qm;	// Queue of methods
	vector 	<RESIDUE> 	resid; 	// data and parameters for each residue
	vector 	<ATOM> 		atom;	// atom coordinates
	opts			mo;

	// input file keywords
	void	readfile (const char *);
	void 	_METHOD	(const vector <string> &);
	void 	_FUNCTION (const vector <string> &);
	int	_VECTOR (const vector <string> &);
	void 	_CONST	(const vector <string> &);
	void 	_SET	(const vector <string> &);
	int 	_RESID	(const vector <string> &);
	void 	__DATA	(const int, const vector <string> &); 

	// usage: resid[work_resid].f[work_func].p[x]
	void    assign_work (int, int);
	void 	synthetic (gsl_rng *);
	int 	work_func;  
	fptr    work_fptr;
	int     work_resid;
	int     work_np;
	int     work_nx;
	int     work_nv;
	double	work_chisq;
	bool	work_synth;		// true when MC simulations
	vector  <double> work_x;
	vector  <double> work_y;
	vector  <double> work_dy;
	vector  <int> work_z;
	vector  <PARAMETER> work_p;
	vector  <PARAMETER> work_v;

	void 	get_x_range (double &, double &);
	int    get_grid_num (int);
	double  get_grid_val (int,int);

	void    set_v (gsl_vector *);
	void    set_v_rng  (gsl_vector *, gsl_rng *);
	void    set_v_qrng (gsl_vector *, gsl_qrng *);
	void    set_pb (vecd &, vecb &, const gsl_vector *);
	void    set_bc (gsl_vector *, gsl_vector *);

	void    set_jacob (gsl_matrix *, const vecd &);
	void    set_grad  (gsl_vector *, const vecd &);

	void    store_v (const gsl_vector *,const gsl_matrix *,const double &);
	void    store_mc (const int, const vector <double> &, 
		const vector < vector < PARAMETER> > &);
	void    select (criterion);
 	bool	inBound (const FUNCTION &);
	void    printout ();
	void 	print_table ();
  };

#endif
