/*
    eMF Copyright 2008-2009 Sung-Hun Bae

    This file is part of eMF.

    eMF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    eMF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with eMF.  If not, see <http://www.gnu.org/licenses/>
*/

#include "dataset.h"
#include "funclib.h"
#include "methods.h"
#include "util.h"

using namespace std;

double grid (void *data)
{
  dataset *D = (dataset *)data;
  int nv = D->work_nv;
  if (nv == 0) return -1.;

  double chisq = -1.;
  gsl_vector *v = gsl_vector_alloc (nv);
  gsl_vector *v_best = gsl_vector_alloc (nv);

  recursive_grid (v, &chisq, v_best, 0, data);

  D->store_v (v_best, NULL, chisq);

  gsl_vector_free (v);
  gsl_vector_free (v_best);
  return chisq;
}

//
// recurseve N-dimensional grid search
//
void recursive_grid (gsl_vector *v, double *chisq, gsl_vector *m, 
  int depth, void *data)
{
  dataset *D = (dataset *)data;
  if (depth == v->size) 
	return;
  else 	{
	for (int j=0;j<D->get_grid_num(depth);j++) {
		gsl_vector_set (v, depth, D->get_grid_val(depth,j));
		recursive_grid (v, chisq, m, depth+1, data);
		if (depth == v->size-1) {
			double x2 = f_chisq (v, data);
#ifdef DEBUG
	for(int qq=0;qq<v->size;qq++) 
		printf("%.1f ",gsl_vector_get(v,qq));
	printf(" %g\n",x2);
#endif
			if (!isnan(x2) && (*chisq < 0. || *chisq > x2)) {
				*chisq = x2;
				gsl_vector_memcpy (m, v);
				} // if
			}
		} // for j
	return;
	}; // depth = 0 .. (nv-1)
}
