@c -----------------------------------------------------------------------------
@c $Id: conmin.texi,v 1.11 2005/05/19 21:07:59 biloti Exp $
@c -----------------------------------------------------------------------------

@cindex minimization, constrained
@cindex conmin

@set conmin @sc{conmin}

This chapter describes the common interfaces for constrained
minimization of arbitrary multidimensional functions.  The library
provides implementations and reimplementations of methods published in
well known optimization and applied mathematics journals.  The library
follows, as close as possible, the GSL standards and tries to use the
GSL routines, specially those ones for optimization, described at
chapters 31, 32, 33, 34, 35 and 36 of the GSL Reference Manual,
version 1.4.

In order to fit the same standards of the GSL multidimensional
minimization, only one iteration, for each method, is
implemented. This means that in case of reimplemented codes, to obtain
the same results of the original version, the user must provide a main
routine that calls the OOL method and checks for optimality, as well
as for stopping criteria, exactly as in the original implementation.

The header file @file{ool_conmin.h} contains prototypes for the
minimization functions and related declarations.

@menu
* Choosing the Minimization Method::
* Initialization and Finalization::
* Providing a function to minimize::
* Providing the constraints::
* CONMIN Iteration::
* CONMIN Stopping Criteria::
* CONMIN Algorithm::
* CONMIN References and Further Reading::
@end menu

@c -----------------------------------------------------------------------------

@node Choosing the Minimization Method
@section Choosing the Minimization Method

Initially the user must define two variables: @var{T} indicates the
method used and @var{P} stands for its parameters. For example, if you
choose to use the Projected Gradient Methods (@code{pgrad}), the code lines
are

@example
  /* Method structs */
  const ool_conmin_minimizer_type *T = ool_conmin_minimizer_pgrad;
  ool_conmin_pgrad_parameters P;
@end example

@c -----------------------------------------------------------------------------

@node Initialization and Finalization
@section Initialization and Finalization

The following function initializes a multidimensional constrained
minimizer.  The minimizer itself depends only on the dimension of the
problem and the algorithm and can be reused for different problems.

@deftypefun {ool_conmin_minimizer *} ool_conmin_minimizer_alloc (const ool_conmin_minimizer_type *@var{T}, size_t @var{n})
This function returns a pointer to a newly allocated instance of a
minimizer of type @var{T} for an @var{n}-dimension function.  If there
is insufficient memory to create the minimizer then the function
returns a null pointer and the error handler is invoked with an error
code of @code{OOL_ENOMEM}.
@end deftypefun

@deftypefun void ool_conmin_parameters_default (const ool_conmin_minimizer_type * @var{T}, void * @var{P})
This function initializes the parameters @var{P} with its default
values, for algorithm @var{T}.
@end deftypefun

@deftypefun int ool_conmin_minimizer_set (ool_conmin_minimizer * @var{M}, ool_conmin_function *@var{fdf}, ool_conmin_constraint * @var{C}, const gsl_vector * @var{x}, void * @var{P})
This function initializes the minimizer @var{M} to minimize the
function @var{fdf} subject to the constraints @var{C}, starting from
the initial point @var{x}. The parameters for the method @var{M} are
given in @var{P}.  Note that for some methods, the initial point
@var{x} must be feasible.
@end deftypefun

@deftypefun void ool_conmin_minimizer_free (ool_conmin_
minimizer *@var{M})
This function frees all the memory associated to the minimizer
@var{M}.
@end deftypefun

@deftypefun {const char *} ool_conmin_minimizer_name (const ool_conmin_minimizer * @var{M})
This function returns a pointer to the name of the minimizer.  For
example,

@example
printf ("s is a '%s' minimizer\n",
        ool_conmin_minimizer_name (s));
@end example
@noindent
would print something like @code{s is a 'pgrad' minimizer}.
@end deftypefun

@c -----------------------------------------------------------------------------

@node Providing a function to minimize
@section Providing a function to minimize

You must provide a parametric function of @math{n} variables for the
minimizer to operate on.  You may also need to provide a routine which
calculates the gradient of the function and a third routine which
calculates both the function value and the gradient together. Some
methods may require a routine to calculate the Hessian times a given
vector.  In order to allow for general parameters the functions are
defined by the following data type:

@deftp {Data Type} ool_conmin_function
This data type defines a general function of @math{n} variables with
parameters and the corresponding gradient vector of derivatives, as
well as the product of the Hessian matrix by a vector.

@table @code
@item size_t n
the dimension of the system, i.e. the number of components of the
vectors @var{x}.

@item double (* f) (const gsl_vector * @var{x}, const void * @var{params})
This function should return the result
@c{$f(x,\hbox{\it params})$}
@math{f(x,params)} for argument @var{x} and parameters @var{params}.

@item void (* df) (const gsl_vector * @var{x}, const void * @var{params}, gsl_vector * @var{g})
This function should store the @var{n}-dimensional gradient @math{g_i
= \partial f(x,\hbox{\it params }) / \partial x_i} in the vector
@var{g} for argument @var{x} and parameters @var{params}, returning an
appropriate error code if the function cannot be computed.

@item void (* fdf) (const gsl_vector * @var{x}, const void * @var{params}, double * f, gsl_vector * @var{g})
This function should set the values of the @var{f} and @var{g} as above,
for arguments @var{x} and parameters @var{params}.  This function provides
an optimization of the separate functions for @math{f(x)} and @math{g(x)} --
it is always faster to compute the function and its gradient at the
same time.

@item void (* Hv) (const gsl_vector *@var{x}, const void * @var{params}, const gsl_vector * @var{V}, gsl_vector * @var{hv} )
This function should return the value @var{hv} of the Hessian matrix
times the vector @var{v}, for arguments @var{x} and parameters
@var{params}. @emph{For some methods, this routine may be required}.

@item void * params
A pointer to the parameters of the function.
@end table
@end deftp
The following example function defines a simple quadratic with parameters,

@example
double
quadratic( const gsl_vector *X, void* params )
@{
  size_t ii, nn;
  double f, x;

  nn = X->size;

  f = 0;

  for( ii = 0; ii < nn; ii++ )
    @{
      x  = gsl_vector_get( X, ii );
      f += (ii+1) * gsl_pow_2( x );
    @}

  return f;
@}

void
quadratic_df( const gsl_vector *X, void* params, gsl_vector *G )
@{
  size_t ii, nn;
  double xx, gg;

  nn = X->size;

  for( ii = 0; ii < nn; ii++ )
    @{
      xx = gsl_vector_get( X, ii );
      gg = 2.0 * (ii+1) * xx;
      gsl_vector_set( G, ii, gg );
    @}
@}

void
quadratic_fdf( const gsl_vector *X, void* params,
	       double *f, gsl_vector *G )
@{
  size_t ii, ip1, nn;
  double xx, gg;

  nn = X->size;
  (*f) = 0;

  for( ii = 0; ii < nn; ii++ )
    @{
      ip1 = ii + 1;

      xx = gsl_vector_get( X, ii );

      (*f) += ip1 * gsl_pow_2( xx );
      gg = 2.0 * ip1 * xx;

      gsl_vector_set( G, ii, gg );
    @}
@}

void
quadratic_Hv( const gsl_vector *X, void *params,
	      const gsl_vector *V, gsl_vector *hv )
@{
  size_t ii, nn;
  double aux;

  nn = X->size;

  for( ii = 0; ii < nn; ii++ )
    @{
      aux = 2.0 * (ii+1) * gsl_vector_get( V, ii );
      gsl_vector_set( hv, ii, aux );
    @}
@}
@end example

The function structure can be initialized using the following code,
@example
 ool_conmin_function F;

 F.n   = nn;
 F.f   = &quadratic;
 F.df  = &quadratic_df;
 F.fdf = &quadratic_fdf;
 F.Hv  = &quadratic_Hv;
 F.params = NULL;
@end example

@c -----------------------------------------------------------------------------

@node Providing the constraints
@section Providing the constraints

You must provide the lower and upper bound of the box constraints for
the minimizers to operate on.

@deftp {Data Type} ool_conmin_constraint
This data type defines a general simple bound constraints for @math{n}
variables.

@table @code
@item size_t n
the dimension of the system, i.e. the number of components of the
vectors @var{x}.

@item gsl_vector * L
@item gsl_vector * U
box constraints.

@c @item size_t ng
@c @item size_t nh
@c number of (nonlinear) inequality and equality constraints.

@c @item double (* g) (const size_t @var{i}, const gsl_vector * @var{x}, const vo@c id * @var{g_params})
@c this function should set the @math{i}-th inequality constraint for
@c argument @var{x} and parameters @var{g_params}, returning an appropriate
@c error code if the function cannot be computed.

@c @item double (* h) (const size_t @var{i}, const gsl_vector * @var{x}, const void * @var{h_params})
@c this function should set the @math{i}-th equality constraint for
@c argument @var{x} and parameters @var{h_params}, returning an appropriate
@c error code if the function cannot be computed.

@c @item void * g_params
@c @item void * h_params
@c a pointer to the parameters of @var{g} and @var{h}.

@end table
@end deftp

The constraints can be initialized using the following code,
@example
 ool_conmin_constraint  C;
 C.n  = nn;

 C.L = gsl_vector_alloc( nn );
 C.U = gsl_vector_alloc( nn );

 gsl_vector_set_all( C.L,   1.0 );
 gsl_vector_set_all( C.U,  10.0 );
@end example

@c -----------------------------------------------------------------------------

@node CONMIN Iteration
@section Iteration

The following function drives the iteration of each algorithm.  The
function performs one iteration to update the state of the minimizer.
The same function works for all minimizers so that different methods can
be substituted at runtime without modifications to the code.

@deftypefun int ool_conmin_minimizer_iterate (ool_conmin_minimizer *@var{M})
The function performs a single iteration of the minimizer @var{M}.  If
the iteration encounters an unexpected problem then an error code will
be returned.
@end deftypefun

@deftypefun int ool_conmin_minimizer_restart (ool_conmin_minimizer *@var{M})
This function resets the minimizer @var{M} to use the current point as a
new starting point.
@end deftypefun

@noindent
The method parameters can be changed between the iterations, using the
functions

@deftypefun void ool_conmin_parameters_get (const ool_conmin_minimizer *M, void  *param_view)
@deftypefunx int ool_conmin_parameters_set (ool_conmin_minimizer *M, void  *new_param)
The first one returns the parameters @var{param_view} used in the
previous iteration, and the second one sets the new parameters
@var{new_param}.
@end deftypefun

@noindent
The minimizer maintains a current best estimate of the minimum at all
times.  This information can be accessed through the following
auxiliary functions,

@deftypefun {gsl_vector *} ool_conmin_minimizer_x (const ool_conmin_minimizer * @var{M})
@deftypefunx{gsl_vector *} ool_conmin_minimizer_dx (const ool_conmin_minimizer * @var{M})
@deftypefunx double ool_conmin_minimizer_minimum (const ool_conmin_minimizer * @var{M})
@deftypefunx {gsl_vector *} ool_conmin_minimizer_gradient (const ool_conmin_minimizer * @var{M})
@deftypefunx double ool_conmin_minimizer_size (const ool_conmin_minimizer * @var{M})
These functions return the current best estimate of the location of
the minimum, the last step, the value of the function at that point,
its gradient, and minimizer specific characteristic size for the
minimizer @var{M}, respectively.
@end deftypefun

@c -----------------------------------------------------------------------------
@node CONMIN Stopping Criteria
@section Stopping Criteria

A minimization procedure should stop when one of the following
conditions is true:

@itemize @bullet
@item
A minimum has been found to within the user-specified precision.

@item
An error has occurred.
@end itemize

@noindent
The handling of these conditions is under user control.  The functions
below permit the user to test the precision of the current result.

@deftypefun int ool_conmin_is_optimal (ool_conmin_minimizer * @var{M})
This functions checks for optimality and is method dependent. The test
returns @code{OOL_SUCCESS} if the following condition is achieved, and
@code{OOL_CONTINUE} otherwise.
@end deftypefun

@deftypefun size_t ool_conmin_minimizer_fcount (const ool_conmin_minimizer * @var{M})
@deftypefunx size_t ool_conmin_minimizer_gcount (const ool_conmin_minimizer * @var{M})
@deftypefunx size_t ool_conmin_minimizer_hcount (const ool_conmin_minimizer * @var{M})
These functions return, respectively, the number of objective
function, gradient and product Hessian-vector evaluations.
@end deftypefun

@c -----------------------------------------------------------------------------
@node CONMIN Algorithm
@section Algorithms

Presently, there are only algorithms for simple bound constraints.
They use the value of the function and most of its gradient at each
evaluation point, too.

@deffn {Minimizer} ool_conmin_minimizer_pgrad
@cindex box-constrained minimization
This is a natural extension of the steepest descent algorithm to bound
constraints problems. In each step, a line search is perfomed along
the projected gradient direction.
@end deffn

@deffn {Minimizer} ool_conmin_minimizer_spg
@cindex box-constrained minimization
@cindex Spectral projected gradient method
@cindex line search, nonmonotone
This is the classical projected gradient method extended to a include
nonmonotone line search strategy and the spectral steplength, which
greatly improves the convergence rate of the method.
@end deffn

@deffn {Minimizer} ool_conmin_minimizer_gencan
@cindex box-constrained minimization
@cindex numerical methods
@cindex active-set strategies
@cindex Spectral projected gradient method
This is a active-set method for smooth box-constrained
minimization. The algorithm combines an unconstrained method,
including a line search which aims to add many constraints to the
working set at a single iteration, with a technique (spectral
projected gradient) for dropping constraints from the working set.
@end deffn

@c -----------------------------------------------------------------------------
@node CONMIN References and Further Reading
@section References and Further Reading
@noindent
A brief description of constrained minimization algorithms and further
references can be found in the following references.

@itemize @asis
@item D.P. Bertsekas,
@cite{Nonlinear Programming}, Athena Scientific, 1999.
@end itemize
@noindent

@itemize @asis
@item D.P. Bertsekas,
@cite{Constrained Optimization and Lagrange Multiplier Methods},
Athena Scientific, 1982.
@end itemize
@noindent

@itemize @asis
@item R. Fletcher,
@cite{Practical Methods for Optimization}, John Wiley and Sons, 1987.
@end itemize
@noindent

@itemize @asis
@item C.T. Kelley,
@cite{Iterative Methods for Optimization}, Frontiers in Applied
Mathematics, SIAM, 1999.
@end itemize
@noindent
