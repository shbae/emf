#!/usr/bin/perl

use Math::Trig;

$eta_xy = 3.345;

print "METHOD QRAND 100\n";
print "METHOD LEVMAR\n";
print "FUNCTION Tanh 1\n";
print "SET Tanh eta_xy 0 10\n";
print "RESIDUE 1\n";
$t = 0.01;
while ($t < 0.05) {
	$y = tanh($eta_xy*$t);
	printf "DATA 0 %.3f %f %f\n", $t, $y, $y*0.05;
	$t += 0.001;
	}
