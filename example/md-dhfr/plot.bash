#!/bin/bash
out="md-dhfr-LEVMAR.out"
eMFextract.pl $out S2s S2f \
| awk '/^[^#]/{printf("%4d %7.3f %7.3f\n",$1,$2*$4,$3*$4+$2*$5)}' > ${out}.S2
eMFextract.pl $out Rex > ${out}.Rex
eMFextract.pl $out model > ${out}.model
# For comparison with MD (Jianhan et al.)
# effective te = (S2f-S2)/(1.0-S2) * te
# effective te(err) = (S2f-S2)/(1.0-S2) * te(err)
eMFextract.pl $out \
| awk '/^[^#]/{S2s=$2;S2f=$8;S2=S2s*S2f;fac=(S2f-S2)/(1.0-S2); print $1,1000*fac*$4,1000*fac*$5}' > ${out}.te
