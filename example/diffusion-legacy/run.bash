#!/bin/bash
# truncated dataset for diffusion test
../../legacy/eMF -A -c dhfr-diffusion.conf dhfr-test.dat

# diffusion
#../../legacy/eMF -A -c dhfr-diffusion.conf dhfr.dat

# model fitting with fixed diffusion parameters
#../../legacy/eMF -A -c dhfr.conf dhfr.dat
