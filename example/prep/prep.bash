#!/bin/bash
cat << END_OF_HEADER
# user accessible constants
#  gamma_h, gamma_x : gyromagnetic ratios for H and X
#  r_xh             : bond length betwen H and X
#  csa_x            : chemical shift anisotropy of X
#  beta             : angle between bond H-X and principal axis of CSA tensor

CONST gamma_h	  26.7519  # (10^7 rad s^-1 T^-1)
CONST gamma_x	  -2.71    # (10^7 rad s^-1 T^-1)
CONST r_xh         0.997   # (A)
CONST csa_x	-170       # (ppm)
CONST beta 	  18       # (deg)

# availabel methods:
#
#   GRID            : grid search
#   QRAND [#]       : quasi-random search (GNU Scientific Library)
#   RAND [#]        : random search (GNU Scientific Library)
#   RANDSIMPLEX [#] : random simplex search (GNU Scientific Library)
#   #= number of trials (optional)
#
#   LEVMAR  [#]     : Levenberg-Marquardt  (GNU Scientific Library)
#   LEVMAR-ML [#]   : Levenberg-Marquardt (Manolis Lourakis)
#   BCOOL [#]       : Box Constrained (Open Optimization Library)
#   CONJGR [#]      : conjugated gradient (GNU Scientific Library)
#   SIMPLEX [#]     : simplex (GNU Scientific Library)
#   SA [#]          : simulated annealing (GNU Scientific Library)
#   #= number of Monte Carlo simulations (optional)
 
# recommended : GRID -> SIMPLEX -> LEVMAR or BCOOL 
#   parameter space is searched grid and simplex (non-gradient method) 
#   then gradient-based algorithms such as Levenberg-Marquardt are applied

METHOD GRID
METHOD SIMPLEX
METHOD LEVMAR
#METHOD LEVMAR-ML
#METHOD BCOOL
#METHOD CONJGR

# bond vector information for axially symmetric diffusion tensor 
VECTOR N HN dhfr.rot.pdb 

# available functions
#   LSI   : extended Lipari-Szabo Isotropic diffusion tensor
#           [S2s, te, Rex, S2f, tc]
#   LSX   : extended Lipari-Szabo aXially symmetric diffusion tensor 
#           [S2s, te, Rex, S2f, tc, Dr, phi, theta]
#   LSICC : extended Lipari-Szabo Isotropic diffusion tensor /w Cross Correlation
#           [S2s, te, Rex, S2f, S2cd, beta, tc]
#   LSXCC : extended Lipari-Szabo aXially symmetric diffusion tensor /w CrosslCorrelation
#           [S2s, te, Rex, S2f, S2cd, beta, tc, Dr, phi, theta]
#   CC    : Cole-Cole spectral density function
#           [tc, e, S2, te, Rex]
#   Loc2  : Local diffusion tensor with two correlation times
#           [tc1, tc2, c, S2, te, Rex]
#   LocX1 : Local aXially symmetric type 1
#           [Dpar, Dper, cos2, S2, te, Rex]
#   LocX2 : Local aXially symmetric type 1
#           [tiso, Dr, cos2, S2, te, Rex]
#   Exp   : exponential decay
#           [R, a, b]
#   Tanh  : hyperbolic tangent
#           [eta_xy]
#         
# bitwise flag for each parameter
#       1 : fitting
#       0 : fixed

FUNCTION LSX 10000000 11000000 10100000 11100000 11010000 # 5 extended Lipari-Szabo models

# parameters are fitted within lower boundary/upper boundary/number of grids(optional)
SET LSX S2s	0  1 20
SET LSX S2f	0  1 20
SET LSX te	0  2 40
SET LSX Rex	0 10 10 
# parameters are fixed by giving a single value
SET LSX tc	9
SET LSX	Dr	1.2
SET LSX	phi	0
SET LSX	theta	0

# RESID <residue number>
# DATA  <data type> <x> <y> <dy(error)>
# data type is handled within each function and can be any integer
# currently,
#   type 0 : R1 (1/s)
#        1 : R2 (1/s)
#        2 : heteronuclear NOE
#        3 : transverse DD-CSA cross correlation rate (eta_xy) (1/s)
#        4 : longitudinal DD-CSA cross correlation rate (eta_z) (1/s)

END_OF_HEADER

eMFprep.pl \
	-d 0 500 r1.500 \
	-d 1 500 r2.500 \
	-d 2 500 noe.500
