#!/usr/bin/perl

use Math::Trig;

$R = 7.351;
$a = 12.5;
$b = 0.0;

print "METHOD QRAND 100\n";
print "METHOD LEVMAR\n";
print "FUNCTION Exp 110 111\n";
print "SET Exp R 0 20\n";
print "SET Exp a 0 20\n";
print "SET Exp b 0 10\n";
print "RESIDUE 1\n";

$t = 0.01;
while ($t < 0.035) {
	$y = $a*exp(-$R*$t)+$b;
	printf "DATA 0 %.3f %f %f\n", $t, $y, $y*0.01;
	$t += 0.001;
	}
